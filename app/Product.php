<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [
        'id','created_at','updated_at',
    ];

    public function brand()
    {
        return $this->belongsTo('App\brand');
    }

    function hasImage() {
        if($this->img_route == '' || $this->img_route == null) {
            return false;
        }
        return true;
    }

    function getThumbnailUrl() {
        return $this->img_route;
    }
}
