<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if(\Auth::check() && $user->role_id < 3) {
            return view('user',compact('user'));
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($user->id == Auth::user()->id) {
            $params = $this->validar($request);
            $user->name = $request->get('name');
            $user->surname1 = $request->get('surname1');
            $user->surname2 = $request->get('surname2');
            $user->date_of_birth = $request->get('date_of_birth');
            $user->zip_code = $request->get('zip_code');
            $user->gender = $request->get('gender');
            $user->marital_status = $request->get('marital_status');
            $user->num_children = $request->get('num_children');
            $user->current_car = $request->get('current_car');
            $user->eco_interest = $request->get('eco_interest');
            $user->annual_income = $request->get('annual_income');
            $user->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validar($request) {
        $fields = $request->validate( [
            'name' => 'required|min:2|max:191',
            'surname1' => 'required|min:2|max:191',
            'surname2' => 'nullable|min:2|max:191',
            'date_of_birth' => 'nullable|date',
            'zip_code' => 'nullable|integer|max:52006',
            'gender' => 'nullable',
            'marital_status' => 'nullable',
            'num_children' => 'nullable|integer|min:0',
            'current_car' => 'nullable',
            'eco_interest' => 'nullable',
            'annual_income' => 'nullable|integer|min:0',
        ]);
        return $fields;
    }
}
