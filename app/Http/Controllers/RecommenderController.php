<?php

namespace App\Http\Controllers;
use DB;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\watched_cars;

class RecommenderController extends Controller
{

    public function get_user_recommendations() {
        if(!Auth::check()) return;
        $this->index();
        return DB::table('recommended_cars')->where('id_user',Auth::user()->id)->get();
    }

    public function index() {
        if(!Auth::check()) return;
        $id_user = Auth::user()->id;
        $watched_cars = watched_cars::where('id_user',$id_user)->where('noted',0)->where('created_at', '>', date("Y-m-d H:i:s", strtotime("-3 hours")))->get();
        if($watched_cars->count() == 0) return;

        $products = new Product;
        $this->export_csv($products->getTable(), $products);
        $watched_cars = new watched_cars;
        $watched_cars->where('id_user',$id_user)->where('created_at', '>', date("Y-m-d H:i:s", strtotime("-3 hours")));
        $this->export_csv($watched_cars->getTable(), $watched_cars);

        $process = new Process(['../recommender/env/bin/python3', '../recommender/knn.py', $id_user]);
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $car_ids = $this->get_string_between($process->getOutput(), '[', ']');
        if($car_ids == "") return;
        $car_ids_array = explode(', ', $car_ids);

        $recommended_cars = array();
        foreach($car_ids_array as $car_id) {
            $recommended_cars[] = ['id_user' => $id_user, 'id_car' => $car_id];
        }

        // Se borran las recomendaciones pasadas antes de añadir las nuevas
        DB::table('recommended_cars')->where('id_user',$id_user)->delete();
        DB::table('recommended_cars')->insert($recommended_cars);

        $watched_cars_query = watched_cars::where('id_user',$id_user)->where('noted',0)->where('created_at', '>', date("Y-m-d H:i:s", strtotime("-3 hours")));
        // Se pone el flag "noted" de la tabla watched_cars a TRUE para que no generen una nueva recomendación
        $watched_cars_query->update(['noted' => 1]);
    }

    function get_string_between($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function export_csv($table,$query) {
        $rows = $query->get();

        $file = fopen('../recommender/' . $table . '.csv', 'w');
        $fields = DB::select('show columns from ' . $table );
        $fields_names = [];
        foreach($fields as $field) {
            $fields_names[] = $field->Field;
        }
        fputcsv($file, $fields_names);
        foreach ($rows as $row) {
            fputcsv($file, $row->toArray());
        }
        fclose($file);
    }
}
