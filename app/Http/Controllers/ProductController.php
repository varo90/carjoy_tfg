<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RecommenderController;
use App\Product;
use App\car_types;
use App\brand;
use App\watched_cars;
use DB;

class ProductController extends Controller
{

    public function carlisting(Request $request)
    {
        $recommended = array();
        if(Auth::check()) {
            $recommend = new RecommenderController();
            $recommended = $recommend->get_user_recommendations()->pluck('id_car')->toArray();
            $recommended = $this->get_products($request)->whereIn('id', $recommended)->get();
        }
        $products = $this->get_products($request)->whereNotIn('id', $recommended)->inRandomOrder()->paginate(10);
        return view('carlisting', [
            'recommended'=> $recommended,
            'products'   => $products,
            'filters'    => $request->all()
        ]);
    }

    public function show(Product $product)
    {
        return view('product.show', ['product' => $product]);
    }

    public function get_products(Request $request) {
        $products = Product::where('active',1);
        $params = $request->all();
        if(count($params) > 0) {
            if($request->has('estado') && $request->get('estado') != 'Todos') {
                $products->where('status',$request->get('estado'));
            }
            if($request->has('km_min') || $request->has('km_max')) {
                if($request->get('km_min') > 0 && $request->get('km_max') > 0) {
                    $products->whereBetween('kilometers', [$request->get('km_min'),$request->get('km_max')]);
                }
            }
            if($request->has('tipo') && $request->get('tipo') != 'Todos') {
                $products->where('id_car_type',car_types::select('id')->where('name',$request->get('tipo'))->first()->id);
            }
            if($request->has('puertas') && $request->get('puertas') != 'Todos') {
                $products->where('num_doors',$request->get('num_doors'));
            }
            if($request->has('marca') && $request->get('marca') != 'Todas') {
                $products->where('brand_id',brand::select('id')->where('name',$request->get('marca'))->first()->id);
            }
            if($request->has('fabricacion_min') || $request->has('fabricacion_max')) {
                if($request->get('fabricacion_min') > 0 && $request->get('fabricacion_max') > 0) {
                    $products->whereBetween('manufacturing_year', [$request->get('fabricacion_min'),$request->get('fabricacion_max')]);
                }
            }
            if($request->has('combustible') && $request->get('combustible') != 'Todos') {
                $products->where('fuel',$request->get('combustible'));
            }
            if($request->has('cv_min') || $request->has('cv_max')) {
                if($request->get('cv_min') > 0 && $request->get('cv_max') > 0) {
                    $products->whereBetween('horsepower', [$request->get('cv_min'),$request->get('cv_max')]);
                }
            }
            if($request->has('cambio') && $request->get('cambio') != 'Todos') {
                $products->where('gear_shift',$request->get('cambio'));
            }
            if($request->has('co2_min') || $request->has('co2_max')) {
                if($request->get('co2_min') > 0 && $request->get('co2_max') > 0) {
                    $products->whereBetween('co2_emission', [$request->get('co2_min'),$request->get('co2_max')]);
                }
            }
        }
        return $products;
    }

    public function getProduct($id) {
        if(Auth::check()) {
            watched_cars::create(['id_user'=> Auth::user()->id, 'id_car'=>$id]);
        }
        return Product::find($id);
    }
}
