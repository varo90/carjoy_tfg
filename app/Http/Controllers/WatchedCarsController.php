<?php

namespace App\Http\Controllers;

use App\watched_cars;
use Illuminate\Http\Request;

class WatchedCarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewed_cars = watched_cars::All();
        return view('admin.viewed_cars',compact('viewed_cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new_viewed_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = watched_cars::create([
            'id_user' => $request->id_user,
            'id_car' => $request->id_car
        ]);
        return back();
                // ->with('success','User Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\watched_cars  $watched_cars
     * @return \Illuminate\Http\Response
     */
    public function show(watched_cars $watched_cars)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\watched_cars  $watched_cars
     * @return \Illuminate\Http\Response
     */
    public function edit(watched_cars $watched_cars)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\watched_cars  $watched_cars
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, watched_cars $watched_cars)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\watched_cars  $watched_cars
     * @return \Illuminate\Http\Response
     */
    public function destroy(watched_cars $watched_cars)
    {
        //
    }
}
