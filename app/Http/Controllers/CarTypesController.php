<?php

namespace App\Http\Controllers;

use App\car_types;
use Illuminate\Http\Request;

class CarTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_types = car_types::All();
        return view('admin/car_types',compact('car_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string|min:2|max:255',
        ]);
        car_types::create(['name'=>$input['name']]);

        return redirect('/admin3012/car-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\car_types  $car_types
     * @return \Illuminate\Http\Response
     */
    public function show(car_types $car_types)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\car_types  $car_types
     * @return \Illuminate\Http\Response
     */
    public function edit(car_types $car_types)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\car_types  $car_types
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, car_types $car_type)
    {
        $name = $request->get('attr_name');
        $car_type->$name = $request->get('attr_value');
        $car_type->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\car_types  $car_types
     * @return \Illuminate\Http\Response
     */
    public function destroy(car_types $car_type)
    {
        $car_type->delete();
    }
}
