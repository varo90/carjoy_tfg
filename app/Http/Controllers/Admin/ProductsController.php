<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Product;
use DB;

class ProductsController extends Controller
{
    public function index() {
        $products = \DB::table('products')->paginate(10);

        return view('admin/products', compact('products'));
    }

    public function create()
    {
        return view('admin/new_product',['product'=>false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'title' => ['required', 'string', 'min:2', 'max:75'],
            'brand_id' => ['required', 'integer'],
            'id_car_type' => ['required', 'integer'],
            'short_description' => ['required', 'string', 'max:300'],
            'description' => ['required', 'string', 'max:2000'],
            'img_route' => ['required','mimes:jpeg,jpg,png,gif'],
            'quantity' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'num_doors' => ['required', 'integer'],
            'manufacturing_year' => ['required', 'integer'],
            'fuel' => ['required', 'string', 'min:2', 'max:15'],
            'status' => ['required', 'string', 'min:2', 'max:15'],
            'kilometers' => ['required', 'integer'],
            'horsepower' => ['required', 'integer'],
            'gear_shift' => ['required', 'string', 'min:2', 'max:15'],
            'fuel_consumption_at_100' => ['required', 'numeric'],
            'co2_emission' => ['required', 'numeric']
        ]);
        $fields['slug'] = Str::slug($fields['title']);

        $imageName = time().'.'.$request->img_route->getClientOriginalExtension();
        $request->img_route->move(public_path('images/cars'), $imageName);
        $fields['img_route'] = 'images/cars/'.$imageName;

        $product = Product::create($fields);
        return back()
                ->with('success','User Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin/product',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin/product',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $name = $request->get('attr_name');
        $product->$name = $request->get('attr_value');
        $product->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function upload_image($id, Request $request) {
        // dd('asdf');
        // $request->validate([
        //     'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        // ]);
        $imageName = time().'.'.$request->img_route->getClientOriginalExtension();

        $request->img_route->move(public_path('images/cars'), $imageName);

        $product = Product::find($id);
        $product->img_route = 'images/cars/'.$imageName;
        $product->save();

        return back()
            ->with('success','You have successfully upload image.')
            ->with('image',$imageName);

        // return back()->with('success','Image Upload successfully');
        // return redirect('/admin3012/products/'.$product_id);
    }

    public function delete_image($id) {
        Product_image::find($id)->delete();
    }

    public function update_categories() {
        try {
            $product_id = request()['product_id'];
            $categories = request()['ids'];
            CategoryProduct::where('product_id',$product_id)->delete();
            
            if($categories != null) {
                foreach($categories as $category_id) {
                    CategoryProduct::create(['category_id'=>$category_id,'product_id'=>$product_id]);
                }
            }
            return 'Categorías actualizadas';
        } catch (\Illuminate\Database\QueryException $e) {
            return $e;
        }
    }
}
