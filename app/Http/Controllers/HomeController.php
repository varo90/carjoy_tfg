<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use App\Product;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = new Request();
        $products = new ProductController();
        if(Auth::check()) {
            $recommend = new RecommenderController();
            $recommended = $recommend->get_user_recommendations()->pluck('id_car')->toArray();
            if(!empty($recommended) && count($recommended) > 0) {
                $products = $products->get_products($request)->whereIn('id', $recommended);
            }
        } 
        if(!Auth::check() || count($recommended) == 0) {
            $products = $products->get_products($request)->inRandomOrder();
        }
        $products = $products->get();

        return view('home', [
            'products'   => $products
        ]);
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact', ['sent' => 0]);
    }

    public function contacted()
    {
        return view('contact', ['sent' => 1]);
    }
}
