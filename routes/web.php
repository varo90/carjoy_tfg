<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact', 'HomeController@contacted')->name('contact');
Route::get('/carlisting', 'ProductController@carlisting')->name('carlisting');
Route::get('/get_product_info/{id}', 'ProductController@getProduct');
Route::get('/recommender', 'RecommenderController@index')->name('recommender');

Route::resource('user', 'UserController');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['prefix' => 'admin3012', 'middleware' => ['admin']], function()
{
	Route::get('/', 'Admin\ProductsController@index');
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products/images/{id}/upload_pic', 'Admin\ProductsController@upload_image');
    Route::resource('brands', 'Admin\BrandsController');
    Route::resource('car-types', 'CarTypesController');
    Route::resource('users', 'Admin\UsersController');
	Route::resource('viewed', 'WatchedCarsController');
});