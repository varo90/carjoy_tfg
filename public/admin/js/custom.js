$('select.product_change').on('change', function() {
    edit_attribute($(this),'products');
});
$("input.product_change").on('keyup', function (e) {
    if (e.keyCode === 13) {
        edit_attribute($(this),'products');
    }
});
$("button.product_change").on('click', function (e) {
    edit_attribute($(this).parent().find('textarea'),'products');
});
$( "input:checkbox" ).on('change', function () {
    if($(this).hasClass('product_change')) {
        var attr_name = $(this).attr('name');
        var value = $(this).is(":checked") ? 1 : 0;
        ajax_call('/admin3012/products/'+$(this).data('id'),'PATCH',{'attr_name':attr_name,'attr_value':value},'text','',0);
        window.location.replace(window.location.href);
    }
    if($(this).is(":checked")) {
        $('#activo').text("Activo");
    } else {
        $('#activo').text("Inactivo");
    }
});

$("input.brand_change").on('keyup', function (e) {
    if (e.keyCode === 13) {
        edit_attribute($(this),'brands');
    }
});
$('.delete_brand').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        ajax_call('/admin3012/brands/'+$(this).data('id'),'DELETE',{},'text','',0);
    }
    window.location.replace(window.location.href);
});
$("input.car_type_change").on('keyup', function (e) {
    if (e.keyCode === 13) {
        edit_attribute($(this),'car-types');
    }
});
$('.delete_type').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        ajax_call('/admin3012/car-types/'+$(this).data('id'),'DELETE',{},'text','',0);
    }
    window.location.replace(window.location.href);
});

$('select.user_change').on('change', function() {
    edit_attribute($(this),'users');
});
$("input.user_change").on('keyup', function (e) {
    if (e.keyCode === 13) {
        edit_attribute($(this),'users');
    }
});
$('.delete_user').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        ajax_call('/admin3012/users/'+$(this).data('id'),'DELETE',{},'text','',0);
    }
    window.location.replace(window.location.href);
});

$('th.orderable').on('click',function() {
    var url = $(location).text().split('?');
    window.location.replace(url[0]+'?order_by='+$(this).data('field'));
});

function edit_attribute(element,model) {
    var attr_name = element.attr('name');
    var value = element.val();
    ajax_call('/admin3012/'+model+'/'+element.data('id'),'PATCH',{'attr_name':attr_name,'attr_value':value},'text','',0);
    window.location.replace(window.location.href);
}


function ajax_call(url,type,data,dataType,display_div,position) {
    data._token = $("meta[name='csrf-token']").attr("content");
    $.ajax(
    {
        url: url,
        type: type,
        dataType: dataType,
        // async: false,
        data: data,
        success: function (response){
            if(display_div != '') {
                $(display_div)[position](response); // = $(display_div).html(response);
            }
        },
        error : function(xhr, status) {
            alert('Disculpe, existió un problema');
        }
    });
}