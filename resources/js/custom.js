$(document).on('click', '.filtrar', filter);
$(document).on('click', '.update_user', update_user);
$(document).on('click', '.watch_product', watch_product);

function filter() {
    window.location.replace('/carlisting'+join_params(get_params()));
}

function update_user() {
    var user_id = $('input#user_id').val();
    ajax_call('/user/'+user_id,'PATCH',get_params(),'text','',0);
    window.location.replace('/user/'+user_id);
}

function watch_product() {
    var product_id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    $.ajax(
    {
        url: '/get_product_info/'+product_id,
        type: 'GET',
        dataType: 'JSON',
        async: false,
        data: {_token:token},
        success: function (response){
            bootbox_show_product(response);
        },
        error : function(xhr, status) {
            alert('Disculpe, existió un problema');
        }
    });
}

function bootbox_show_product(product) {
    bootbox.alert({
        title: product.title,
        message: '<div><img src="' +product.img_route +'">' + '<br>'
            + '<center>' + '<strong>'
            + product.status + ' | '  
            + product.kilometers + 'Km' + ' | '
            + product.fuel + ' | '
            + product.horsepower + 'Cv' + ' | '
            + product.gear_shift + ' | '
            + product.fuel_consumption_at_100 + 'l/100' + ' | '
            + product.co2_emission + 'l/CO2'
            + '</strong>' + '</center>'
            + '<br><br>'
            + product.short_description + '</p></div>',
        backdrop: true
    });
    // <p>{{ $product->short_description }}</p>
    //             <p>{{ $product->status }} | 
    //                 @if($product->status != 'NUEVO')
    //                     {{ $product->kilometers }}Km |
    //                 @endif
    //                 {{ $product->fuel }} |
    //                 {{ $product->horsepower }}Cv |
    //                 {{ $product->gear_shift }} |
    //                 {{ $product->fuel_consumption_at_100 }}l/100 |
    //                 {{ $product->co2_emission }} l/CO2 |
    //             </p>
}

function get_params() {
    var params = {};
    $('input').each(function(){ params[$(this).attr('name')] = $(this).val(); });
    $('select').each(function(){ params[$(this).attr('name')] = $(this).val() });
    return params;
}

function join_params(params) {
    var params_string = '?';
    var cont = 0;
    if(Object.keys(params).length == 0) return '';
    jQuery.each(params, function(index, item) {
        if(cont > 0) params_string += '&';
        params_string += index + '=' + item;
        cont++;
    });
    return params_string;
}

// ajax_call('/pedidos_productos/create','GET',{},'text','.card .card-body.order_products','append');
// Función genérica para hacer peticiones AJAX
function ajax_call(url,type,data,dataType,display_div,position) {
    data._token = $("meta[name='csrf-token']").attr("content");
    $.ajax(
    {
        url: url,
        type: type,
        dataType: dataType,
        async: false,
        data: data,
        success: function (response){
            if(display_div != '') {
                $(display_div)[position](response); // = $(display_div).html(response);
            }
        },
        error : function(xhr, status) {
            alert('Disculpe, existió un problema');
        }
    });
}