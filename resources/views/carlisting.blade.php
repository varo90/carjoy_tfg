@extends('layouts.app')

{{-- @section('categories-menu')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="categoriesMenu">
            <ul class="navbar-nav">
            @foreach($taxonomies as $taxonomy)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ $taxonomy->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @include('product.index._category_level', ['taxons' => $taxonomy->rootLevelTaxons()])
                    </div>
                </li>
            @endforeach
            </ul>
        </div>
    </nav>
@stop --}}

@section('hero-area')
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title">
                    <h2>Listado de <span>Coches</span></h2>
                    <p>¡Encuentra el tuyo y pide cita para probarlo!</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->

@endsection


@section('content')
<section class="ptb-100 list-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-4">
                <div class="sidebar">
                    <h4 class="search_title">Opciones de búsqueda</h4>
                    <div class="list-sidebar">
                        {{-- <h5>PALABRAS CLAVE</h5>
                        <input type="text" placeholder="Keywords separadas por comas"> --}}
                        <h5>ESTADO</h5>
                        <select name="estado" class="source_param">
                            <option value="Todos" @if(isset($filters['estado']) && $filters['estado'] == 'Todos') selected @endif>Todos</option>
                            <option value="Nuevo" @if(isset($filters['estado']) && $filters['estado'] == 'Nuevo') selected @endif>Nuevo</option>
                            <option value="Km0" @if(isset($filters['estado']) && $filters['estado'] == 'Km0') selected @endif>Km0</option>
                            <option value="Usado" @if(isset($filters['estado']) && $filters['estado'] == 'Usado') selected @endif>Usado</option>
                        </select>
                        <h5>KILÓMETROS entre:</h5>
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="km_min" class="source_param" type="number" min="{{\App\Product::all()->min('kilometers')}}" max="{{\App\Product::all()->max('kilometers')}}" value="{{ isset($filters['km_min']) ? $filters['km_min'] : \App\Product::all()->min('kilometers') }}" placeholder="Km min">
                            </div>
                            <div class="col-lg-6">
                                <input name="km_max" class="source_param" type="number" min="{{\App\Product::all()->min('kilometers')}}" max="{{\App\Product::all()->max('kilometers')}}" value="{{ isset($filters['km_max']) ? $filters['km_max'] : \App\Product::all()->min('kilometers') }}" placeholder="Km max">
                            </div>
                        </div>
                        <h5>TIPO DE VEHÍCULO</h5>
                        <select name="tipo" class="source_param">
                            <option value="Todos" @if(isset($filters['tipo']) && $filters['tipo'] == 'Todos') selected @endif>Todos</option>
                            @foreach(\App\car_types::all() as $car_type)
                                <option value="{{$car_type->name}}" @if(isset($filters['tipo']) && $filters['tipo'] == $car_type->name) selected @endif>{{$car_type->name}}</option>
                            @endforeach
                        </select>
                        <h5>Nº PUERTAS</h5>
                        <select name="puertas" class="source_param">
                            <option value="Todos" @if(isset($filters['puertas']) && $filters['puertas'] == 'Todos') selected @endif>Todos</option>
                            <option value="3" @if(isset($filters['puertas']) && $filters['puertas'] == '3') selected @endif>3</option>
                            <option value="5" @if(isset($filters['puertas']) && $filters['puertas'] == '5') selected @endif>5</option>
                        </select>
                        <h5>MARCA</h5>
                        <select name="marca" class="source_param">
                            <option value="Todas" @if(isset($filters['marca']) && $filters['marca'] == 'Todas') selected @endif>Todas</option>
                            @foreach(\App\brand::all() as $brand)
                                <option value="{{$brand->name}}" @if(isset($filters['marca']) && $filters['marca'] == $brand->name) selected @endif>{{$brand->name}}</option>
                            @endforeach
                        </select>
                        <h5>AÑO DE FABRICACIÓN entre:</h5>
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="fabricacion_min" class="source_param" type="number" min="{{\App\Product::all()->min('manufacturing_year')}}" max="{{\App\Product::all()->max('manufacturing_year')}}" value="{{ isset($filters['fabricacion_min']) ? $filters['fabricacion_min'] : \App\Product::all()->min('manufacturing_year') }}" placeholder="Año min">
                            </div>
                            <div class="col-lg-6">
                                <input name="fabricacion_max" class="source_param" type="number" min="{{\App\Product::all()->min('manufacturing_year')}}" max="{{\App\Product::all()->max('manufacturing_year')}}" value="{{ isset($filters['fabricacion_max']) ? $filters['fabricacion_max'] : \App\Product::all()->max('manufacturing_year') }}" placeholder="Año max">
                            </div>
                        </div>
                        <h5>COMBUSTIBLE</h5>
                        <select name="combustible" class="source_param">
                            <option value="Todos" @if(isset($filters['combustible']) && $filters['combustible'] == 'Todos') selected @endif>Todos</option>
                            <option value="Gasolina" @if(isset($filters['combustible']) && $filters['combustible'] == 'Gasolina') selected @endif>Gasolina</option>
                            <option value="Diesel" @if(isset($filters['combustible']) && $filters['combustible'] == 'Diesel') selected @endif>Diésel</option>
                            <option value="Hibrido" @if(isset($filters['combustible']) && $filters['combustible'] == 'Hibrido') selected @endif>Híbrido</option>
                            <option value="Electrico" @if(isset($filters['combustible']) && $filters['combustible'] == 'Electrico') selected @endif>Eléctrico</option>
                        </select>
                        <h5>POTENCIA (CV) entre:</h5>
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="cv_min" class="source_param" type="number" min="{{\App\Product::all()->min('horsepower')}}" max="{{\App\Product::all()->max('horsepower')}}" value="{{ isset($filters['cv_min']) ? $filters['cv_min'] : \App\Product::all()->min('horsepower') }}" placeholder="Caballos min">
                            </div>
                            <div class="col-lg-6">
                                <input name="cv_max" class="source_param" type="number" min="{{\App\Product::all()->min('horsepower')}}" max="{{\App\Product::all()->max('horsepower')}}" value="{{ isset($filters['cv_max']) ? $filters['cv_max'] : \App\Product::all()->max('horsepower') }}" placeholder="Caballos max">
                            </div>
                        </div>
                        <h5>CAMBIO</h5>
                        <select name="cambio" class="source_param">
                            <option value="Todos" @if(isset($filters['cambio']) && $filters['cambio'] == 'Todos') selected @endif>Todos</option>
                            <option value="Manual" @if(isset($filters['cambio']) && $filters['cambio'] == 'Manual') selected @endif>Manual</option>
                            <option value="Automatico" @if(isset($filters['cambio']) && $filters['cambio'] == 'Automatico') selected @endif>Automático</option>
                        </select>
                        <h5>EMISIONES CO2 (g/km) entre:</h5>
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="co2_min" class="source_param" type="number" min="{{\App\Product::all()->min('co2_emission')}}" max="{{\App\Product::all()->max('co2_emission')}}" value="{{ isset($filters['co2_min']) ? $filters['co2_min'] : \App\Product::all()->min('co2_emission') }}" placeholder="Emisiones min">
                            </div>
                            <div class="col-lg-6">
                                <input name="co2_max" class="source_param" type="number" min="{{\App\Product::all()->min('co2_emission')}}" max="{{\App\Product::all()->max('co2_emission')}}" value="{{ isset($filters['co2_max']) ? $filters['co2_max'] : \App\Product::all()->max('co2_emission') }}" placeholder="Emisiones max">
                            </div>
                        </div>
                        <h5></h5>
                        <div class="row">
                            <div class="col-lg-6">
                                <a class="theme-btn filtrar">Filtrar</a>
                            </div>
                            <div class="col-lg-6">
                                <a href="/carlisting" class="theme-btn">Todos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                @if(!empty($recommended) && count($recommended) > 0)
                    <center><h2><i class="icofont icofont-hand-down"></i> RECOMENDADOS PARA TI <i class="icofont icofont-hand-down"></i></h2></center>
                        @foreach($recommended as $product)
                            @include('product.index._product')
                        @endforeach
                    <center><h2><i class="icofont icofont-hand-up"></i>RECOMENDADOS PARA TI<i class="icofont icofont-hand-up"></i></h2></center>
                @endif
                @if(!empty($products))
                    @foreach($products as $product)
                        @include('product.index._product')
                    @endforeach
                @endif
                <div class="shop-pagination box-shadow text-center ptblr-10-30">
                    {{ $products->links() }}
                </div>
            </div>

        </div>
    </div>
</section><!-- blog section end -->
@endsection
