@extends('admin/layouts/layout')

@section('title','Productos')
@section('title_page','Productos')

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div id="updated_correctly"></div>
    <div class="table-responsive">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                @include('admin._product_header_table')
            </thead>
            <tfoot>
                @include('admin._product_header_table')
            </tfoot>
            <tbody>
                @foreach($products as $product)
                    @include('admin._product_row')
                @endforeach
            </tbody>
        </table>
        <div class="shop-pagination box-shadow text-center ptblr-10-30">
            {{ $products->links() }}
        </div>
        <div>
            <a href="/admin3012/products/create" class="btn btn-primary btn-icon-split f-right" target="_blank">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Añadir nuevo</span>
            </a>
        </div>
    </div>

@endsection