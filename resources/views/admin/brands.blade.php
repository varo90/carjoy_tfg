@extends('admin/layouts/layout')

@section('title','Productos')
@section('title_page','Marcas')

@section('content')

    <div class="row">
        <div class="col-lg-6">
            @foreach($brands as $brand)
                <div class="card">
                    <div class="card-header lightgrey">
                        <h6 class="m-0 card_text font-weight-bold text-primary">
                            <input data-id="{{$brand->id}}" type="text" name="name" class="brand_change mb-2" value="{{$brand->name}}">
                        </h6>
                        <button data-id="{{$brand->id}}" class="btn btn-danger delete_brand f-right"><i class="fas fa-times"></i></button>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="mt-40">
        <div>
            <form class="form-events" method="POST" action="/admin3012/brands" enctype="multipart/form-data">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="name" value="" placeholder="Nombre de la nueva marca...">
                </div>
                {{-- <div class="form-group">
                    <input required type="file" name="image" placeholder="Marca" multiple>
                </div> --}}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Crear</span>
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection