@extends('admin/layouts/layout')

@section('title','Productos')
@section('title_page','Productos')

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div id="updated_correctly"></div>
    <div class="table-responsive">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Coche</th>
                    <th>Usuario</th>
                </tr>
            </thead>
            <tfoot>
                <th>Coche</th>
                <th>Usuario</th>
            </tfoot>
            <tbody>
                @foreach($viewed_cars as $viewed)
                    <tr>
                        <td class="">
                            <select data-id="{{$viewed->id}}" name="id_car_type" class="viewed_change">
                                @foreach(\App\Product::all() as $car)
                                    <option value="{{$car->id}}" @if($viewed->id_car == $car->id) selected @endif>{{$car->id . ': ' . $car->title}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td class="">
                            <select data-id="{{$viewed->id}}" name="id_car_type" class="viewed_change">
                                @foreach(\App\User::all() as $user)
                                    <option value="{{$user->id}}" @if($viewed->id_user == $user->id) selected @endif>{{$user->id . ': ' . $user->name . ' ' . $user->surname1 . ' ' . $user->surname2}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div>
            <a href="{{url('/admin3012/viewed/create')}}" class="btn btn-primary btn-icon-split f-right" target="_blank">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Añadir nuevo</span>
            </a>
        </div>
    </div>

@endsection