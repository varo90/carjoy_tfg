@include('admin/layouts/header');

@include('admin/layouts/left_menu')

@include('admin/layouts/navbar')

@yield('content')

@include('admin/layouts/footer');