@extends('admin/layouts/layout')

@section('title','Producto '.$product->slug)
@section('title_page','Editar ' . $product->name)

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div id="updated_correctly"></div>
    <div class="table-responsive mt-40">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                @include('admin._product_header_table')
            </thead>
            <tfoot>
                @include('admin._product_header_table')
            </tfoot>
            <tbody>
                @include('admin._product_row')
            </tbody>
        </table>
    </div>

    <div class="container">
        <div class="row mt-20">
            <form action="/admin3012/products/images/{{$product->id}}/upload_pic" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <input type="file" name="img_route" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="row mt-20">
            <label for="exampleFormControlTextarea1">Descripción corta:</label>
            <textarea data-id="{{$product->id}}" name="short_description" class="form-control" max="300" id="short_description" rows="3">{{$product->short_description}}</textarea>
            <button class="product_change btn btn-primary" type="submit">Editar</button>
        </div>

        <div class="row mt-20">
            <label for="exampleFormControlTextarea1">Descripción:</label>
            <textarea data-id="{{$product->id}}" name="description" class="form-control" max="2000" id="description" rows="6">{{$product->description}}</textarea>
            <button class="product_change btn btn-primary" type="submit">Editar</button>
        </div>
    </div>
    

@endsection