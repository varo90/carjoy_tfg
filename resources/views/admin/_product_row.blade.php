<tr>
    <td class=""><a href="/admin3012/products/{{$product->id}}"><img src="{{ $product->img_route != NULL ? URL::asset($product->img_route) : URL::asset('img/product/1.jpg') }}" width="70"></a></td>
    <td class="">{{ $product->id }} </td>
    <td class="">
        <input data-id="{{$product->id}}" name="title" class="product_change" type="text" value="{{ $product->title }}" placeholder="Título">
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="brand_id" class="product_change">
            @foreach(\App\brand::all() as $brand)
                <option value="{{$brand->id}}" @if($product->brand_id == $brand->id) selected @endif>{{$brand->name}}</option>
            @endforeach
        </select>
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="id_car_type" class="product_change">
            @foreach(\App\car_types::all() as $car_type)
                <option value="{{$car_type->id}}" @if($product->id_car_type == $car_type->id) selected @endif>{{$car_type->name}}</option>
            @endforeach
        </select>
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="status" class="product_change">
            <option value="NUEVO" @if($product->status == 'NUEVO') selected @endif>Nuevo</option> 
            <option value="KM0" @if($product->status == 'KM0') selected @endif>Km0</option>
            <option value="USADO" @if($product->status == 'USADO') selected @endif>Usado</option>
        </select>
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="kilometers" class="product_change" type="number" value="{{ $product->kilometers }}" placeholder="Kilómetros">
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="num_doors" class="product_change">
            <option value="3" @if($product->num_doors == 3) selected @endif>3</option>
            <option value="5" @if($product->num_doors == 5) selected @endif>5</option>
        </select>
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="quantity" class="product_change" type="number" value="{{ $product->quantity }}" placeholder="Cantidad">
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="price" class="product_change" type="number" value="{{ $product->price }}" placeholder="Precio">
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="manufacturing_year" class="product_change" type="number" value="{{ $product->manufacturing_year }}" placeholder="Año">
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="fuel" class="product_change">
            <option value="GASOLINA" @if($product->fuel == 'GASOLINA') selected @endif>Gasolina</option>
            <option value="DIESEL" @if($product->fuel == 'DIESEL') selected @endif>Diésel</option>
            <option value="HIBRIDO" @if($product->fuel == 'HIBRIDO') selected @endif>Híbrido</option>
            <option value="ELECTRICO" @if($product->fuel == 'ELECTRICO') selected @endif>Eléctrico</option>
        </select>
    </td>
    <td class="">
        <select data-id="{{$product->id}}" name="gear_shift" class="product_change">
            <option value="MANUAL" @if($product->gear_shift == 'MANUAL') selected @endif>Manual</option>
            <option value="AUTOMATICO" @if($product->gear_shift == 'AUTOMATICO') selected @endif>Automático</option>
        </select>
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="horsepower" class="product_change" type="number" value="{{ $product->horsepower }}" placeholder="CV">
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="fuel_consumption_at_100" class="product_change" type="number" value="{{ $product->fuel_consumption_at_100 }}" placeholder="Consumo/100">
    </td>
    <td class="">
        <input data-id="{{$product->id}}" name="co2_emission" class="product_change" type="number" value="{{ $product->co2_emission }}" placeholder="Emisión CO2">
    </td>
    <td class="">
        <div class="custom-control custom-switch">
            <input data-id="{{$product->id}}" name="active" type="checkbox" class="custom-control-input product_change" id="active_{{$product->id}}" @if($product->active == 1) checked @endif>
            <label class="custom-control-label" for="active_{{$product->id}}"><span id="activo">@if($product->active == 1) Activo @else Inactivo @endif</span></label>
        </div>
        {{-- <div class="custom-control custom-switch">
            <input data-id="{{$product->id}}" name="active" type="checkbox" class="custom-control-input product_change" id="active" @if($product->active == 1) checked @endif>
            <label class="custom-control-label" for="customSwitches"><span id="activo">@if($product->active == 1)Activo @else Inactivo @endif</span></label>
        </div> --}}
    </td>
    {{-- <td>{{ date('H:i:s d/m/Y',strtotime($product->created_at)) }}</td>
    <td>{{ date('H:i:s d/m/Y',strtotime($product->updated_at)) }}</td> --}}
</tr>