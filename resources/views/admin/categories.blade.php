@extends('admin/layouts/layout')

@section('title','Productos')
@section('title_page','Categorías de productos')

@section('content')

    <div class="row">
        <div class="col-lg-6">
            @foreach($categories as $category)
                <div class="card">
                    <div class="card-header lightgrey">
                        <h6 class="m-0 card_text font-weight-bold text-primary">
                            <input data-id="{{$category->id}}" type="text" name="name" class="changeable_category mb-2" value="{{$category->name}}">
                        </h6>
                        <button data-id="{{$category->id}}" class="btn btn-danger delete_category f-right"><i class="fas fa-times"></i></button>
                    </div>
                    <div class="card-body">
                        @foreach(\App\Category::where('parent_id',$category->id)->get() as $subcategory)
                            <div class="card">
                                <div class="card-header lightgrey">
                                    <h6 class="m-0 card_text font-weight-bold text-primary">
                                        <input data-id="{{$subcategory->id}}" type="text" name="name" class="changeable_category mb-2" value="{{$subcategory->name}}">
                                    </h6>
                                    <button data-id="{{$subcategory->id}}" class="btn btn-danger delete_category f-right"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="mt-40">
        <div>
            <form class="form-events" method="POST" action="/admin3012/categories" enctype="multipart/form-data">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="text" name="name" value="" placeholder="Nombre de la nueva categoría...">
                </div>
                <div class="form-group">
                    <label for="parent_id">Categoría padre:
                        <select id="parent_id" name="parent_id">
                            <option value='0'>---</option>
                            @foreach($categories as $category)
                                <option value='{{$category->id}}'>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
                {{-- <div class="form-group">
                    <input type="file" name="image" placeholder="Categoría" multiple>
                </div> --}}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Crear</span>
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection