@extends('admin/layouts/layout')

@section('title','Nueva visualización')
@section('title_page','Nueva visualización')

@section('content')
<div class="container">
    <form id="new_viewed_form" method="POST" action="/admin3012/viewed" enctype="multipart/form-data">
        @csrf
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @foreach($errors->all() as $error)
            <span class="validation_error">{{$error}}</span>
        @endforeach
        <select name="id_user">
            @foreach(\App\User::all() as $user)
                <option value="{{$user->id}}">{{$user->id . ': ' . $user->name . ' ' . $user->surname1 . ' ' . $user->surname2}}</option>
            @endforeach
        </select>
        <select name="id_car">
            @foreach(\App\Product::all() as $car)
                <option value="{{$car->id}}">{{$car->id . ': ' . $car->title}}</option>
            @endforeach
        </select>
        <button class="btn btn-primary btn-icon-split mt-20">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Crear</span>
        </button>
    </form>
</div>
@endsection