@extends('admin/layouts/layout')

@section('title','Cuentas de usuarios')
@section('title_page','Cuentas de usuarios')

@section('content')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div id="updated_correctly"></div>
    <div class="table-responsive">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                @include('admin._user_header_table')
            </thead>
            <tfoot>
                @include('admin._user_header_table')
            </tfoot>
            <tbody>
                @foreach($users as $user)
                    @include('admin._user_row')
                @endforeach
            </tbody>
        </table>
        <div class="shop-pagination box-shadow text-center ptblr-10-30">
            {{ $users->links() }}
        </div>
        <div>
            <a href="/admin3012/users/create" class="btn btn-primary btn-icon-split f-right" target="_blank">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Añadir nuevo</span>
            </a>
        </div>
    </div>

@endsection