@extends('admin/layouts/layout')

@section('title','Nuevo producto')
@section('title_page','Nuevo producto')

@section('content')
<div class="container">
    <form id="new_product_form" method="POST" action="/admin3012/products" enctype="multipart/form-data">
        @csrf
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @foreach($errors->all() as $error)
            <span class="validation_error">{{$error}}</span>
        @endforeach
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="title">Nombre:</label>
                <input id="title" name="title" class="form-control" type="text" placeholder="Título" value="{{old('title')}}" max="75" required>
                <span class="validation_error">{{$errors->first('title')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="brand_id">Marca:</label>
                <select id="brand_id" name="brand_id" class="form-control">
                    @foreach(\App\brand::all() as $brand)
                        <option value="{{$brand->id}}" @if(old('brand_id') == $brand->id) selected @endif>{{$brand->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="img_route">Imagen:</label>
                <input id="img_route" type="file" name="img_route" class="form-control" required>
                <span class="validation_error">{{$errors->first('img_route')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="short_description">Descripción corta:</label>
                <textarea id="short_description" name="short_description" class="form-control" max="300" rows="3">{{old('short_description')}}</textarea>
                <span class="validation_error">{{$errors->first('short_description')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="description">Descripción:</label>
                <textarea id="description" name="description" class="form-control" max="2000" rows="6">{{old('description')}}</textarea>
                <span class="validation_error">{{$errors->first('description')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="id_car_type">Tipo:</label>
                <select id="id_car_type" name="id_car_type" class="form-control">
                    @foreach(\App\car_types::all() as $car_type)
                        <option value="{{$car_type->id}}" @if(old('car_types') == $car_type->id) selected @endif>{{$car_type->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="status">Estado:</label>
                <select id="status" name="status" class="form-control">
                    <option value="NUEVO" @if(old('status') == "NUEVO") selected @endif>Nuevo</option> 
                    <option value="KM0" @if(old('status') == "KM0") selected @endif>Km0</option>
                    <option value="USADO" @if(old('status') == "USADO") selected @endif>Usado</option>
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="num_doors">Nº de Puertas:</label>
                <select id="num_doors" name="num_doors" class="form-control">
                    <option value="3" @if(old('num_doors') == "3") selected @endif>3</option>
                    <option value="5" @if(old('num_doors') == "5") selected @endif>5</option>
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="quantity">Cantidad:</label>
                <input id="quantity" name="quantity" class="form-control" type="number" placeholder="Cantidad" value="{{old('quantity')}}" min="0">
                <span class="validation_error">{{$errors->first('quantity')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="kilometers">Kilómetros:</label>
                <input id="kilometers" name="kilometers" class="form-control" type="number" placeholder="Kilómetros" value="{{old('kilometers')}}" min="0">
                <span class="validation_error">{{$errors->first('kilometers')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="price">Precio:</label>
                <input id="price" name="price" class="form-control" type="number" min="0" placeholder="Precio" value="{{old('price')}}">
                <span class="validation_error">{{$errors->first('price')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="manufacturing_year">Año:</label>
                <input id="manufacturing_year" name="manufacturing_year" class="form-control" type="number" min="0" placeholder="Año" value="{{old('manufacturing_year')}}">
                <span class="validation_error">{{$errors->first('manufacturing_year')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="fuel">Combustible:</label>
                <select id="fuel" name="fuel" class="form-control">
                    <option value="GASOLINA" @if(old('fuel') == "GASOLINA") selected @endif>Gasolina</option>
                    <option value="DIESEL" @if(old('fuel') == "DIESEL") selected @endif>Diésel</option>
                    <option value="HIBRIDO" @if(old('fuel') == "HIBRIDO") selected @endif>Híbrido</option>
                    <option value="ELECTRICO" @if(old('fuel') == "ELECTRICO") selected @endif>Eléctrico</option>
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="gear_shift">Cambio de marchas:</label>
                <select id="gear_shift" name="gear_shift" class="form-control">
                    <option value="MANUAL" @if(old('gear_shift') == "MANUAL") selected @endif>Manual</option>
                    <option value="AUTOMATICO" @if(old('gear_shift') == "AUTOMATICO") selected @endif>Automático</option>
                </select>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="horsepower">Potencia en caballos:</label>
                <input id="horsepower" name="horsepower" class="form-control" type="number" min="0" placeholder="CV" value="{{old('horsepower')}}">
                <span class="validation_error">{{$errors->first('horsepower')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="fuel_consumption_at_100">Consumo a los 100:</label>
                <input id="fuel_consumption_at_100" name="fuel_consumption_at_100" class="form-control" type="number" step="0.01" min="0" placeholder="Consumo/100" value="{{old('fuel_consumption_at_100')}}">
                <span class="validation_error">{{$errors->first('fuel_consumption_at_100')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="co2_emission">Emisión de Co2:</label>
                <input id="co2_emission" name="co2_emission" class="form-control" type="number" step="0.01" min="0" placeholder="Emisión CO2" value="{{old('co2_emission')}}">
                <span class="validation_error">{{$errors->first('co2_emission')}}</span>
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-6">
                <label for="co2_emission">Activo:</label>
                <div class="custom-control custom-switch">
                    <input name="active" type="checkbox" class="custom-control-input" id="active" checked>
                    <label class="custom-control-label" for="active"><span id="activo">Activo</span></label>
                </div>
            </div>
        </div>
        <button class="btn btn-primary btn-icon-split mt-20">
            <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
            </span>
            <span class="text">Crear</span>
        </button>
    </form>
</div>
@endsection