<meta name="csrf-token" content="{{ csrf_token() }}">
@csrf
<tr>
    <td class="">
        <button data-id="{{$user->id}}" class="btn btn-danger delete_user f-right"><i class="fas fa-times"></i></button>
    </td>
    <td class="">{{ $user->id }} </td>
    <td class="">
        <input name="email" class="user_change" type="email" placeholder="email" value="{{ $user->email }}">
    </td>
    <td class="">
        <select data-id="{{$user->id}}" name="role_id" class="user_change">
            @foreach(\App\Role::all() as $role)
                <option value="{{$role->id}}" @if($role->id == $user->role_id) selected @endif>{{$role->name}}</option>
            @endforeach
        </select>
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="name" class="user_change" type="text" value="{{ $user->name }}" placeholder="Nombre" required>
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="surname1" class="user_change" type="text" value="{{ $user->surname1 }}" placeholder="Apellido 1" required>
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="surname2" class="user_change" type="text" value="{{ $user->surname2 }}" placeholder="Apellido 2">
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="zip_code" class="user_change" type="text" placeholder="Código postal" value="@if($user->zip_code != null){{ $user->zip_code }}@endif">
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="date_of_birth" class="form-control user_change" type="date" value="@if($user->date_of_birth != null){{ \Carbon::createFromFormat('Y-m-d', $user->date_of_birth)->format('Y-m-d') }}@endif" id="example-date-input">
    </td>
    <td class="">
        <select data-id="{{$user->id}}" name="gender" class="user_change">
            <option value="HOMBRE" @if($user->gender != null && $user->gender == 'HOMBRE') selected @endif>Hombre</option>
            <option value="MUJER"  @if($user->gender != null && $user->gender == 'MUJER') selected @endif>Mujer</option>
        </select>
    </td>
    <td class="">
        <select data-id="{{$user->id}}" name="marital_status" class="user_change">
            <option value="SOLTERO" @if($user->marital_status != null && $user->marital_status == 'SOLTERO') selected @endif>Soltero</option>
            <option value="COMPROMETIDO" @if($user->marital_status != null && $user->marital_status == 'COMPROMETIDO') selected @endif>Comprometido</option>
            <option value="EN_RELACION" @if($user->marital_status != null && $user->marital_status == 'EN_RELACION') selected @endif>En relación</option>
            <option value="CASADO" @if($user->marital_status != null && $user->marital_status == 'CASADO') selected @endif>Casado</option>
            <option value="PAREJA_DE_HECHO" @if($user->marital_status != null && $user->marital_status == 'PAREJA_DE_HECHO') selected @endif>Pareja de hecho</option>
            <option value="SEPARADO" @if($user->marital_status != null && $user->marital_status == 'SEPARADO') selected @endif>Separado</option>
            <option value="DIVORCIADO" @if($user->marital_status != null && $user->marital_status == 'DIVORCIADO') selected @endif>Divorciado</option>
            <option value="VIUDO" @if($user->marital_status != null && $user->marital_status == 'VIUDO') selected @endif>Viudo</option>
        </select>
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="num_children" class="user_change" type="number" min="0" placeholder="Nº de hijos" value="@if(is_numeric($user->num_children)){{$user->num_children}}@endif">
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="current_car" class="user_change" type="text" placeholder="Coche actual" value="@if($user->current_car != null){{ $user->current_car }}@endif">
    </td>
    <td class="">
        <select data-id="{{$user->id}}" name="eco_interest" class="user_change">
            <option value="SI" @if($user->eco_interest != null && $user->eco_interest == 'SI') selected @endif>Sí</option>
            <option value="ALGO" @if($user->eco_interest != null && $user->eco_interest == 'ALGO') selected @endif>Algo</option>
            <option value="NO" @if($user->eco_interest != null && $user->eco_interest == 'NO') selected @endif>No</option>
        </select>
    </td>
    <td class="">
        <input data-id="{{$user->id}}" name="annual_income" class="user_change" type="number" min="0" step="1000" placeholder="Ingresos anuales" value="@if($user->annual_income != null){{$user->annual_income}}@endif">
    </td>
</tr>