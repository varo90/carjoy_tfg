@extends('layouts.app')

@section('hero-area')
<!-- hero area start -->
<section class="hero-area" id="slideslow-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="hero-area-content">
                    <h1>Encuentra tu coche ideal</h1>
                    <h2>Tenemos una gran gama de vehículos que adorarás.</h2>
                    <a href="/carlisting" class="theme-btn">¡ADELANTE!</a>
                    <ul class="nav nav-tabs">
                        <li><a class="active show" data-target="#menu2" data-toggle="tab" href="#">Busca vehículos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu2" class="tab-pane fade active show">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <select name="estado" class="source_param">
                                        <option value="Todos">Estado</option>
                                        <option value="Nuevo">Nuevo</option>
                                        <option value="Km0">Km0</option>
                                        <option value="Usado">Usado</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <select name="marca" class="source_param">
                                        <option value="Todas">Marca</option>
                                        @foreach(\App\brand::all() as $brand)
                                            <option value="{{$brand->name}}">{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <select name="combustible" class="source_param">
                                        <option value="Todos">Todos</option>
                                        <option value="Gasolina">Gasolina</option>
                                        <option value="Diesel">Diésel</option>
                                        <option value="Hibrido">Híbrido</option>
                                        <option value="Electrico">Eléctrico</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <select name="tipo" class="source_param">
                                        <option value="Todos">Tipo</option>
                                        @foreach(\App\car_types::all() as $car_type)
                                            <option value="{{$car_type->name}}">{{$car_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <select name="puertas" class="source_param">
                                        <option value="Todos">Puertas</option>
                                        <option value="3">3</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <a class="theme-btn filtrar">Buscar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- hero area end -->
@endsection


@section('content')

<!-- feature section start -->
<section class="feature-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2>Vehículos <span>Recomendados</span></h2>
                    <p>Nuestros mejores chollos, listos para ti y al alcance de tu mano.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs">
                    <li><a class="active show" data-target="#menu4" data-toggle="tab" href="#">Coches recomendados</a></li>
                    <li><a data-toggle="tab" data-target="#menu5" href="#menu4">Nuevos</a></li>
                    <li><a data-toggle="tab" data-target="#menu6" href="#menu6">Usados / Km0</a></li>
                    {{-- <li><a data-toggle="tab" data-target="#menu7" href="#menu6">Coches usados</a></li> --}}
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div id="menu4" class="tab-pane fade active show">
                <div class="row">
                    @php $displayed = 0 @endphp
                    @foreach($products as $product)
                        @php $displayed++ @endphp
                        @if($displayed > 6) @php break; @endphp @endif
                        <div class="col-lg-4 col-md-6">
                            <div class="single-feature">
                                <div class="feature-image">
                                    <img src="{{$product->img_route}}" alt="{{$product->title}}" style="width:360px;height:200px">
                                    <div class="feature-overlay">
                                        <div class="display-table">
                                            <div class="display-tablecell">
                                                <a data-id={{ $product->id }} class="clickable watch_product"><i class="icofont icofont-eye-alt"></i></a>
                                                <a data-id={{ $product->id }} class="clickable like_product"><i class="icofont icofont-like"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-middle">
                                    <a href="#"><i class="icofont icofont-headphone-alt-1"></i>{{$product->brand->name}}</a>
                                    <a href="#"><i class="icofont icofont-glue-oil"></i>{{$product->fuel}}</a>
                                    <a href="#"><i class="icofont icofont-company"></i>{{$product->gear_shift}}</a>
                                    <a href="#"><i class="icofont icofont-power-zone"></i>{{$product->horsepower}} CV</a>
                                    <a href="#"><i class="icofont icofont-energy-water"></i>Año {{$product->manufacturing_year}}</a>
                                    <a href="#"><i class="icofont icofont-cur-euro"></i>{{$product->price}}</a>
                                </div>
                                <div class="feature-bottom">
                                    <h4>{{$product->title}}</h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="menu5" class="tab-pane fade">
                <div class="row">
                    @php $displayed = 0 @endphp
                    @foreach($products as $product)
                        @if($product->status == 'NUEVO')
                            @php $displayed++ @endphp
                            @if($displayed > 6) @php break; @endphp @endif
                            <div class="col-lg-4 col-md-6">
                                <div class="single-feature">
                                    <div class="feature-image">
                                        <img src="{{$product->img_route}}" alt="{{$product->title}}" style="width:360px;height:200px">
                                        <div class="feature-overlay">
                                            <div class="display-table">
                                                <div class="display-tablecell">
                                                    <a data-id={{ $product->id }} class="clickable watch_product"><i class="icofont icofont-eye-alt"></i></a>
                                                    <a data-id={{ $product->id }} class="clickable like_product"><i class="icofont icofont-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feature-middle">
                                        <a href="#"><i class="icofont icofont-headphone-alt-1"></i>{{$product->brand->name}}</a>
                                        <a href="#"><i class="icofont icofont-glue-oil"></i>{{$product->fuel}}</a>
                                        <a href="#"><i class="icofont icofont-company"></i>{{$product->gear_shift}}</a>
                                        <a href="#"><i class="icofont icofont-power-zone"></i>{{$product->horsepower}} CV</a>
                                        <a href="#"><i class="icofont icofont-energy-water"></i>Año {{$product->manufacturing_year}}</a>
                                        <a href="#"><i class="icofont icofont-cur-euro"></i>{{$product->price}}</a>
                                    </div>
                                    <div class="feature-bottom">
                                        <h4>{{$product->title}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div id="menu6" class="tab-pane fade">
                <div class="row">
                    @php $displayed = 0 @endphp
                    @foreach($products as $product)
                        @if($product->status == 'USADO' || $product->status == 'KM0')
                            @php $displayed++ @endphp
                            @if($displayed > 6) @php break; @endphp @endif
                            <div class="col-lg-4 col-md-6">
                                <div class="single-feature">
                                    <div class="feature-image">
                                        <img src="{{$product->img_route}}" alt="{{$product->title}}" style="width:360px;height:200px">
                                        <div class="feature-overlay">
                                            <div class="display-table">
                                                <div class="display-tablecell">
                                                    <a data-id={{$product->id}} class="clickable watch_product"><i class="icofont icofont-eye-alt"></i></a>
                                                    <a data-id={{$product->id}} class="clickable like_product"><i class="icofont icofont-like"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feature-middle">
                                        <a href="#"><i class="icofont icofont-headphone-alt-1"></i>{{$product->brand->name}}</a>
                                        <a href="#"><i class="icofont icofont-glue-oil"></i>{{$product->fuel}}</a>
                                        <a href="#"><i class="icofont icofont-company"></i>{{$product->gear_shift}}</a>
                                        <a href="#"><i class="icofont icofont-paper-plane"></i>{{$product->kilometers}}</a>
                                        <a href="#"><i class="icofont icofont-energy-water"></i>{{$product->manufacturing_year}}</a>
                                        <a href="#"><i class="icofont icofont-cur-euro"></i>{{$product->price}}</a>
                                    </div>
                                    <div class="feature-bottom">
                                        <h4>{{$product->title}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mt-20 text-center">
                <a href="/carlisting" class="theme-btn theme-btn2">Ver más</a>
            </div>
        </div>
    </div>
</section><!-- feature section end -->


<section class="about-bottom ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="about-bottom-box">
                    <div class="sec-title mb-50">
                        <h2>SOBRE NOSOTROS</h2>
                        <p>CarJoy es un concesionario multimarca donde compramos y vendemos tanto vehículos nuevos como usados siempre al mejor precio para el cliente.</p>
                    </div>
                    <ul>
                        <li>
                            <div class="about-info">
                                <h4>SELLO DE CONFIANZA</h4>
                                <p>Revisamos a fondo los vehículos que compramos para garantizar que son seguros y están en perfectas condiciones. </p>
                            </div>
                            <div class="about-icon">
                                <i class="icofont icofont-stamp"></i>
                            </div>
                        </li>
                        <li>
                            <div class="about-info">
                                <h4>SOMOS MULTIMARCA</h4>
                                <p>Trabajamos con varias marcas para asegurarnos de que con nosotros encuentras lo que buscas. </p>
                            </div>
                            <div class="about-icon">
                                <i class="icofont icofont-box"></i>
                            </div>
                        </li>
                        <li>
                            <div class="about-info">
                                <h4>ATENCIÓN INDIVIDUALIZADA</h4>
                                <p>Tanto si quieres comprar como vender, te damos una atención personalizada para que consigas lo que buscas. </p>
                            </div>
                            <div class="about-icon">
                                <i class="icofont icofont-ui-user"></i>
                            </div>
                        </li>
                        <li>
                            <div class="about-info">
                                <h4>AMIGOS DE LA TECNOLOGÍA</h4>
                                <p>Utilizamos algoritmos de Machine Learning para facilitarte la búsqueda del vehículo que más se adapta a ti. </p>
                            </div>
                            <div class="about-icon">
                                <i class="icofont icofont-computer"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about section end -->

<!-- choose section start -->
<section class="ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2>Elige tu <span>Marca</span></h2>
                    <p>Estos son los vehículos que te recomendamos categorizados por marca</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="choose-menu">
                    <button data-filter="*" class="active">Todas</button>
                    @foreach(\App\brand::all() as $brand)
                        <button data-filter=".{{$brand->id}}">{{$brand->name}}</button>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row choose-item">
            @include('product._search_by_brand')
        </div>
        <div class="row">
            <div class="col-lg-12 mt-20 text-center">
                <a href="/carlisting" class="theme-btn theme-btn2">Ver más</a>
            </div>
        </div>
    </div>
</section><!-- choose section end -->

@endsection
