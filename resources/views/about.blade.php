@extends('layouts.app')

@section('hero-area')
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2>Sobre <span>Nosotros</span></h2>
                    <p>Te contamos sobre nosotros para que te animes a contarnos sobre ti.</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->

@endsection

@section('content')

<!-- about section start -->
<section class="ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2>¿Qué nos caracteriza?</h2>
                    <p>¡Con nosotros aciertas! Ofrecemos el producto que más de adecua a tus necesidades por el mejor precio del mercado.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-about">
                    <i class="icofont icofont-stamp"></i>
                    <h4>SELLO DE CONFIANZA</h4>
                    <p>Revisamos a fondo los vehículos que compramos para garantizar que son seguros y están en perfectas condiciones. </p>
                    {{-- <a class="theme-btn" href="#">Read more</a> --}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-about">
                    <i class="icofont icofont-box"></i>
                    <h4>SOMOS MULTIMARCA</h4>
                    <p>Trabajamos con varias marcas para asegurarnos de que con nosotros encuentras lo que buscas entre una mayor gama. </p>
                    {{-- <a class="theme-btn" href="#">Read more</a> --}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-about">
                    <i class="icofont icofont-ui-user"></i>
                    <h4>ATENCIÓN INDIVIDUALIZADA</h4>
                    <p>Tanto si quieres comprar como vender, te damos una atención personalizada para que consigas lo que buscas. </p>
                    {{-- <a class="theme-btn" href="#">Read more</a> --}}
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-about">
                    <i class="icofont icofont-computer"></i>
                    <h4>AMIGOS DE LA TECNOLOGÍA</h4>
                    <p>Utilizamos algoritmos de Machine Learning para facilitarte la búsqueda del vehículo que más se adapta a ti. </p>
                    {{-- <a class="theme-btn" href="#">Read more</a> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!-- funfact section start -->
<section class="funfact-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8">
                <div class="single-funfact">
                    <div class="sec-title">
                        <h2>HECHOS</h2>
                        <p>Algunos datos que prueban nuestro compromiso con el cliente.</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="funfact-box">
                                <h4>VEHÍCULOS EN STOCK</h4>
                                <h1><i class="icofont icofont-car"></i> <span class="">{{\App\Product::count()}}</span> </h1>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="funfact-box">
                                <h4>MARCAS CON LAS QUE TRABAJAMOS</h4>
                                <h1><i class="icofont icofont-box"></i> <span class="">{{\App\brand::count()}}</span> </h1>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="funfact-box">
                                <h4>CLIENTES SATISFECHOS</h4>
                                <h1><i class="icofont icofont-businessman"></i> <span class="">{{\App\User::where('role_id',2)->count()}}</span> </h1>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="funfact-box">
                                <h4>RATIO DE VENTA / VISITAS</h4>
                                <h1><i class="icofont icofont-trophy"></i> <span class="">1/2</span> </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- funfact section end -->


<!-- start map area -->
<section class="ptb-100">
    <div class="sec-title mb-50">
        <h2>¿Dónde estamos?</h2>
        <p>Ven a visitarnos y prueba el vehículo que más te guste. ¡No te decepcionaremos!</p>
    </div>
    <div class="map-area">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3012.9624136277766!2d-5.672877348792075!3d40.96040223021642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd3f26409c4d5287%3A0x2809ba7ed4a44c2e!2sFacultad%20de%20Ciencias!5e0!3m2!1ses!2ses!4v1590492702102!5m2!1ses!2ses" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        {{-- <div id="map"></div> --}}
    </div>
</section>
<!-- map section end -->

@endsection
