@extends('layouts.app')

@section('hero-area')
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2><span>Contáctanos</span></h2>
                    <p>¿Tienes alguna duda o quieres pedir hora para una prueba? Estamos aquí para ayudarte ¡Escríbenos sin compromiso!</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->

@endsection

@section('content')

<!-- contact section start -->
<div class="ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    @if($sent)
                        <h2><font color="green">Tu mensaje ha sido enviado</font></h2>
                    @else
                        <h2>Déjanos un mensaje</h2>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="contact-us-area">
                    <form action="/contact" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="name" placeholder="Nombre*" required>
                            </div>
                            <div class="col-lg-6">
                                <input type="email" name="email" placeholder="Email*" required>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="subject" placeholder="Asunto*" required>
                            </div>
                            <div class="col-lg-12">
                                <textarea placeholder="Escribe aquí tu mensaje*" required></textarea>
                                <button class="theme-btn theme-btn2" type="submit" name="replysubmit">Enviar<button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- contact section end -->

@endsection
