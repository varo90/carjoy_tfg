<div class="single-post listing-post">
    <div class="row flexbox-center">
        <div class="col-sm-5">
            <div class="post-thumbnail">
                {{-- <a href="{{ route('product.show', $product) }}"> --}}
                <a data-id={{$product->id}} class="clickable watch_product">
                    <img class="card-img-top"
                    @if($product->hasImage())
                        src="{{ $product->getThumbnailUrl() }}"
                    @else
                        src="/images/product.jpg"
                    @endif
                    alt="{{ $product->name }}" />
                </a>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="post-details">
                {{-- <a href="{{ route('product.show', $product) }}"> --}}
                <a data-id={{$product->id}} class="clickable watch_product">
                    <h4 class="post-title">{{ $product->title }}</h4>
                </a>
                <p>{{ $product->short_description }}</p>
                <p>{{ $product->status }} | 
                    @if($product->status != 'NUEVO')
                        {{ $product->kilometers }}Km |
                    @endif
                    {{ $product->fuel }} |
                    {{ $product->horsepower }}Cv |
                    {{ $product->gear_shift }} |
                    {{ $product->fuel_consumption_at_100 }}l/100 |
                    {{ $product->co2_emission }} l/CO2 |
                </p>
                <a data-id={{$product->id}} class="clickable watch_product theme-btn theme-btn2">{{ number_format($product->price,2,',','.') }} €</a>
            </div>
        </div>
    </div>
</div>