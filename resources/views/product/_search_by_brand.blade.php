@foreach($products as $i => $product)
    @if($i >= 12) @php break; @endphp @endif
    <div class="col-lg-4 col-md-6 {{$product->brand_id}}">
        <div class="single-choose">
            <div class="feature-image">
            <img src="{{$product->img_route}}" alt="{{$product->title}}" style="width:360px;height:200px">
                <div class="feature-overlay">
                    <div class="display-table">
                        <div class="display-tablecell">
                            <a data-id={{ $product->id }} class="clickable watch_product"><i class="icofont icofont-eye-alt"></i></a>
                            <a data-id={{ $product->id }} class="clickable like_product"><i class="icofont icofont-like"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="choose-content">
                <h4>{{$product->title}}<span>{{$product->price}} €</span></h4>
                <a href=""><i class="icofont icofont-clock-time"></i><p>{{$product->manufacturing_year}}</p></a>
                <a href=""><i class="icofont icofont-energy-oil"></i><p>{{$product->fuel}}</p></a>
                <a href=""><i class="icofont icofont-chart-flow"></i><p>{{$product->gear_shift}}</p></a>
                <a href=""><i class="icofont icofont-car-alt-1"></i><p>{{$product->horsepower}} CV</p></a>
            </div>
        </div>
    </div>
@endforeach