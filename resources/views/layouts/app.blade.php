<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- @include('layouts._favicons') --}}
</head>
<body>
    <div id="preloader">
        <div class="display-table">
            <div class="display-tablecell">
                <img src="{{ url('images/loader.gif') }}" alt="">
            </div>
        </div>
    </div>
    <!-- header section start -->
    <div class="top-area">
        <div class="head-top-area">
            <header class="header-top border-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 d-none d-sm-block">
                            <div class="header-top-content text-lg-left text-center">
                                <ul>
                                    <li><i class="icofont icofont-clock-time"></i>Lun - Vie : 09:00 a 18:00 pm</li>
                                    <li><a href="tel:+8801524212235"><i class="icofont icofont-telephone"></i>(+880) 567-290-4567</a></li>
                                    <li><a href="mailto:contact@carjoy.com"><i class="icofont icofont-envelope"></i>contact@carjoy.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="header-top-content login-reg text-lg-right text-center"> 
                                <ul>
                                    @guest
                                        <li><a href="{{ route('login') }}"><i class="icofont icofont-user"></i> Accede </a></li>
                                        <li><a href="{{ route('register') }}"><i class="icofont icofont-pencil-alt-2"></i> Regístrate </a></li>
                                    @else
                                    <form>
                                        <li><a href="{{ url('/user/'. Auth::user()->id ) }}"><i class="icofont icofont-user"></i> ¡Hola {{ Auth::user()->name }}! </a></li>
                                        <li><a href="{{ route('logout') }}"><i class="icofont icofont-logout"></i> Salir </a></li>
                                    </form>
                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="header-bottom">
                <div class="container">
                    <div class="row flexbox-center">
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="logo text-lg-left text-center">
                                <a href="/"><img src="{{ url('images/logo.png') }}" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-6">
                            <div class="responsive-menu"></div>
                            <div class="mainmenu">
                                <ul id="primary-menu">
                                    <li class="@if(\Route::current()->getName() == 'home') active @endif"><a href="/">Inicio</a></li>
                                    <li class="@if(\Route::current()->getName() == 'about') active @endif"><a href="/about">Sobre nosotros</a></li>
                                    <li class="@if(strpos(\Route::current()->getName(), 'carlisting')  !== false ) active @endif"><a href="/carlisting">Listado</a></li>
                                    <li class="@if(\Route::current()->getName() == 'contact') active @endif"><a href="/contact">Contacto</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- header section end -->

        @yield('hero-area')
    </div><!-- top-area -->


    @yield('content')

    <!-- footer section start -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="footer-top">
                        <div class="row flexbox-center">
                            <div class="col-xl-8 col-lg-7">
                                <div class="footer-top-left">
                                    <h4>¿Quieres vender un coche?</h4>
                                    <hr>
                                    <p>Contacta con nosotros y valoraremos el estado de tu vehículo para ofrecerte el mejor precio.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-5">
                                <div class="footer-top-right">
                                    <a class="theme-btn theme-btn2" href="/contact">CONTACTAR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer-top flexbox-center">
                        <div class="row flexbox-center">
                            <div class="col-xl-8 col-lg-7">
                                <div class="footer-top-left">
                                    <h4>¿Quieres comprar un coche?</h4>
                                    <hr>
                                    <p>¡Tenemos el ideal para ti! Comprueba los que tenemos disponibles y ven a probarlo sin compromiso.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-5">
                                <div class="footer-top-right">
                                    <a class="theme-btn theme-btn2" href="/carlisting">¡VEÁMOSLO!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-30">
                <div class="col-lg-3 col-md-6">
                    <div class="widget">
                        <a href="index-2.html"><img alt="footer-logo" src="{{ url('images/logo.png') }}"></a>
                        <p>CarJoy es un concesionario multimarca donde compramos y vendemos tanto vehículos nuevos como usados siempre al mejor precio para el cliente.</p>
                        <div class="widget-social-icons">
                            <a href=""><i class="icofont icofont-social-facebook"></i></a>
                            <a href=""><i class="icofont icofont-social-twitter"></i></a>
                            <a href=""><i class="icofont icofont-social-skype"></i></a>
                            <a href=""><i class="icofont icofont-social-pinterest"></i></a>
                            <a href=""><i class="icofont icofont-social-google-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="widget">
                        <h4 class="widget-title">Nuestras marcas</h4>
                        <ul>
                            @foreach(\App\brand::all() as $brand)
                                <li><a href="/carlisting?marca={{$brand->name}}"><i class="icofont icofont-curved-double-right"></i>{{$brand->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="widget">
                        <h4 class="widget-title">Últimas noticias</h4>
                        <div class="widget-gallery">
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget1.jpg') }}"></a>
                            </div>
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget2.jpg') }}"></a>
                            </div>
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget3.jpg') }}"></a>
                            </div>
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget4.jpg') }}"></a>
                            </div>
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget5.jpg') }}"></a>
                            </div>
                            <div class="single-widget-gallery">
                                <a href=""><img alt="widget" src="{{ asset('/images/widget/widget6.jpg') }}"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget-get-in-touch">
                        <h4 class="widget-title">Contáctanos</h4>
                        <ul>
                            <li><i class="icofont icofont-location-pin"></i><p><span>Nuestra dirección : </span>Calle Falsa, 123, Salamanca</p></li>
                            <li><i class="icofont icofont-phone"></i><p><span>Número de teléfono : </span>(+880) 567-290-4567</p></li>
                            <li><i class="icofont icofont-clock-time"></i><p><span>Horario comercial : </span> Lun- Vie : 09:00 a 18:00</p></li>
                            <li><i class="icofont icofont-envelope"></i><p><span>Email: </span>contact@carjoy.com</p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <p><a target="_blank" href="">CarJoy</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer section end -->
    <a href="#" class="scrollToTop">
        <i class="icofont icofont-arrow-up"></i>
    </a>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    {{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKyOpsNq-vWYtrwayN3BkF3b4k3O9A_A&amp;callback=initMap"></script> --}}
    @yield('scripts')
</body>
</html>
