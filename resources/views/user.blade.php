@extends('layouts.app')

@section('hero-area')
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title mb-50">
                    <h2>Tu <span>Cuenta</span></h2>
                    <p>Ésta es tu información de usuario. ¡Cuéntanos más, y más te ayudaremos!</p>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->

@endsection

@section('content')

<!-- user info section start -->
<section class="ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Nombre*:</h5>
                            <input name="name" class="user_field" type="text" placeholder="Nombre*" value="@if(Auth::user()->name != null){{ Auth::user()->name }}@endif" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Apellido 1*:</h5>
                            <input name="surname1" class="user_field" type="text" placeholder="Apellido 1*" value="@if(Auth::user()->surname1 != null){{ Auth::user()->surname1 }}@endif" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Apellido 2:</h5>
                            <input name="surname2" class="user_field" type="text" placeholder="Apellido 2" value="@if(Auth::user()->surname2 != null){{ Auth::user()->surname2 }}@endif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Fecha de nacimiento:</h5>
                            <input name="date_of_birth" class="form-control user_field" type="date" value="@if(Auth::user()->date_of_birth != null){{ \Carbon::createFromFormat('Y-m-d', Auth::user()->date_of_birth)->format('Y-m-d') }}@endif" id="example-date-input">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Estado civil</h5>
                            <select name="marital_status" class="user_field">
                                <option value="SOLTERO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'SOLTERO') selected @endif>Soltero</option>
                                <option value="COMPROMETIDO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'COMPROMETIDO') selected @endif>Comprometido</option>
                                <option value="EN_RELACION" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'EN_RELACION') selected @endif>En relación</option>
                                <option value="CASADO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'CASADO') selected @endif>Casado</option>
                                <option value="PAREJA_DE_HECHO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'PAREJA_DE_HECHO') selected @endif>Pareja de hecho</option>
                                <option value="SEPARADO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'SEPARADO') selected @endif>Separado</option>
                                <option value="DIVORCIADO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'DIVORCIADO') selected @endif>Divorciado</option>
                                <option value="VIUDO" @if(Auth::user()->marital_status != null && Auth::user()->marital_status == 'VIUDO') selected @endif>Viudo</option>
                            </select>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-lg-12">
                            <h5>email:</h5>
                            <input name="email" class="user_field" type="email" placeholder="email" value="@if(Auth::user()->email != null){{ Auth::user()->email }}@endif">
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Género</h5>
                            <select name="gender" class="user_field">
                                <option value="HOMBRE" @if(Auth::user()->gender != null && Auth::user()->gender == 'HOMBRE') selected @endif>Hombre</option>
                                <option value="MUJER"  @if(Auth::user()->gender != null && Auth::user()->gender == 'MUJER') selected @endif>Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="user_id" value="{{Auth::user()->id}}">
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Código postal:</h5>
                            <input name="zip_code" class="user_field" type="text" placeholder="Código postal" value="@if(Auth::user()->zip_code != null){{ Auth::user()->zip_code }}@endif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Nº de hijos:</h5>
                            <input name="num_children" class="user_field" type="number" min="0" placeholder="Nº de hijos" value="@if(is_numeric(Auth::user()->num_children)){{Auth::user()->num_children}}@endif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Coche actual:</h5>
                            <input name="current_car" class="user_field" type="text" placeholder="Coche actual" value="@if(Auth::user()->current_car != null){{ Auth::user()->current_car }}@endif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Interés en el medio ambiente:</h5>
                            <select name="eco_interest" class="user_field">
                                <option value="SI" @if(Auth::user()->eco_interest != null && Auth::user()->eco_interest == 'SI') selected @endif>Sí</option>
                                <option value="ALGO" @if(Auth::user()->eco_interest != null && Auth::user()->eco_interest == 'ALGO') selected @endif>Algo</option>
                                <option value="NO" @if(Auth::user()->eco_interest != null && Auth::user()->eco_interest == 'NO') selected @endif>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Ingresos anuales:</h5>
                            <input name="annual_income" class="user_field" type="number" min="0" step="1000" placeholder="Ingresos anuales" value="@if(Auth::user()->annual_income != null){{Auth::user()->annual_income}}@endif">
                        </div>
                    </div>
                    <a class="theme-btn update_user">Editar</a>
                </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</section>

@endsection
