
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$ = window.jQuery = require('jquery');
// Slicknav JS
require('./jquery.slicknav.min');
// owl carousel JS
require('./owl.carousel.min');
// Popup JS
require('./jquery.magnific-popup.min');
// Counterup JS
require('./jquery.counterup.min');
// Counterup waypoints JS
require('./waypoints.min');
// Isotope JS
require('./isotope.pkgd.min');
// Vega JS
require('./vegas.min');
// main JS
require('./sb-admin-2.min.js');
// Custom
require('./custom');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

//const app = new Vue({
//    el: '#app'
//});
