$('.update_product').on('click',function() {
    var id = $(this).data("id");
    window.open('/admin3012/products/'+id, '_blank');
});
$('.changeable').on('change',function() {
    update_field($(this),'/admin3012/products/');
});
CKEDITOR.replace( 'description' );
CKEDITOR.instances.description.on('blur', function() { //
    update_field($('#description'),'/admin3012/products/');
});
$('.changeable_brand').on('change',function() {
    update_field($(this),'/admin3012/brands/');
});
$('.changeable_category').on('change',function() {
    update_field($(this),'/admin3012/categories/');
});
$('.delete_img').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        delete_($(this),'/admin3012/products/images/','POST');
    }
});
$('.delete_brand').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        delete_($(this),'/admin3012/brands/','DELETE');
    }
});
$('.delete_category').on('click',function() {
    var r = confirm("¿De verdad de la buena?");
    if (r == true) {
        delete_($(this),'/admin3012/categories/','DELETE');
    }
});

$('#pre-selected-categories').multiSelect();
$('#pre-selected-categories').on('change',function(){
    select_ids($(this),'categories');
});

$('#pre-selected-brands').multiSelect();
$('#pre-selected-brands').on('change',function(){
    select_ids($(this),'brands');
});

$('.orderby').on('click',function() {
    var attribute = $(this).data("id");
    var value = $(this).data("order");
    $form = $('#filters');
    $form.append($('<input type="hidden" name="orderby" />').val(attribute));
    $form.append($('<input type="hidden" name="order" />').val(value));
    $form.get(0).submit();
});



function update_field(selector,url) {
    var field = selector.attr('name');
    var value = selector.val();
    if(field == 'description') {
        value = CKEDITOR.instances.description.getData();
    }
    var id = selector.data("id");
    var token = $('#token').val();
    var params = {id:id,field:field,value:value,'_token':token};
    $.ajax(
    {
        url: url+id,
        type: 'PATCH',
        data: params,
        success: function (response){
            // if(field=='undiscounted_sell_price' || field=='percentage_discount' ||  field=='is_main_image') {
                location.reload();
            // } else {
            //     $('#updated_correctly').html(response);
            // }
        }
    });
}

function delete_(selector,url,action) {
    var id = selector.data("id");
    var token = $('#token').val();
    var params = {'_token':token};
    $.ajax(
    {
        url: url+id,
        type: action,
        data: params,
        success: function (response){
            location.reload();
        }
    });
}

function select_ids(selector,type) {
    var ids = get_ids(type);
    var product_id = selector.data("id");
    var token = $('#token').val();
    var params = {product_id:product_id,ids:ids,'_token':token};
    $.ajax(
    {
        url: '/admin3012/products/'+type,
        type: 'POST',
        data: params,
        success: function (response){
            location.reload();
        }
    });
}

function get_ids(type) {
    var ids = [];
    $.each($("#pre-selected-"+type+" option:selected"), function(){            
        ids.push($(this).val());
    });
    return ids;
}