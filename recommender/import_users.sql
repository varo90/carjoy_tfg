-- Database Manager 4.2.5 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '2',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `surname1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `surname2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('HOMBRE','MUJER') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` enum('SOLTERO','COMPROMETIDO','EN_RELACION','CASADO','PAREJA_DE_HECHO','SEPARADO','DIVORCIADO','VIUDO') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_children` int(11) DEFAULT NULL,
  `current_car` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eco_interest` enum('SI','ALGO','NO') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_income` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `role_id`, `name`, `surname1`, `surname2`, `date_of_birth`, `zip_code`, `email`, `email_verified_at`, `password`, `remember_token`, `gender`, `marital_status`, `num_children`, `current_car`, `eco_interest`, `annual_income`, `created_at`, `updated_at`) VALUES
(1,	1,	'Álvaro',	'Santos',	'Martín',	'1990-12-30',	'37002',	'alvaro_sm_8@usal.es',	'2020-06-13 11:26:31',	'$2y$10$c24H4bW/Ge5ZdgyP1H4FcOBdpXHrPCXP5ProyUpMPG19AtRI/9E1G',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Nissan Almera',	'SI',	20000,	'2020-06-13 11:26:31',	'2020-06-13 11:26:31'),
(2,	2,	'Humberto',	'Miss Lia Macejkovic III',	'Kirsten Walter',	'1956-03-26',	'51173',	'heather.west@kirlin.net',	'2018-02-28 21:37:03',	'$2y$10$FPFuD.8FY/d.SIJJGQt//ebhn5gIHiDlbw13EEl1Ds7DYX1HenAnK',	NULL,	'MUJER',	'COMPROMETIDO',	1,	'Mr. Moses Runolfsson',	'SI',	35000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(3,	2,	'Giles',	'Dr. Fredrick Donnelly Jr.',	'Rigoberto Lindgren',	'1993-02-08',	'03396',	'louisa.paucek@yahoo.com',	'2019-03-21 23:30:41',	'$2y$10$eOWR4pUoHAMYAm/LGtignOmGi/.WoeMF5R1hCJ.4kYncLYqdir0G2',	NULL,	'HOMBRE',	'CASADO',	3,	'Dr. Thelma Johnston II',	'NO',	23000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(4,	3,	'Kathlyn',	'Dr. Coby Hermiston V',	'Prof. Lester Bergnaum',	'1967-07-28',	'26643',	'maynard80@yahoo.com',	'2018-05-07 11:44:35',	'$2y$10$3z4oPKvItfSbeSKvWu/DOOoM9KUABxRhJ6cPabEF3VC4jgSU2NUcq',	NULL,	'HOMBRE',	'VIUDO',	3,	'Mr. Marcellus Rowe',	'SI',	22000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(5,	2,	'Demarco',	'Mrs. Sienna Davis',	'Kenya Fadel',	'1954-11-15',	'10362',	'huels.robbie@streich.org',	'2019-03-17 11:19:08',	'$2y$10$qKgGv2ExLvcUjcOK/esK8uM7S7gE/7J3YIAQnTKMd3hjdSAyZAax.',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Tess Hills',	'ALGO',	29000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(6,	3,	'Schuyler',	'Sylvia Friesen',	'Dr. Holden Stark',	'1999-04-06',	'99958',	'juvenal32@yahoo.com',	'2020-05-12 16:39:33',	'$2y$10$rtYJjoTh.k0YbehzMq3lKONtpf6..3Fcen0lt1tDiUKOFO/PyXoDu',	NULL,	'HOMBRE',	'PAREJA_DE_HECHO',	4,	'Americo Turcotte',	'NO',	76000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(7,	3,	'Salma',	'America Fay',	'Javonte Toy',	'1968-03-04',	'27170',	'earlene.jacobi@yahoo.com',	'2019-01-16 04:23:26',	'$2y$10$MTaQrRqiy6m7g2jNqBrHMu3CYbqyqniiqK9/IP0OIZMQOEHV/kT.K',	NULL,	'HOMBRE',	'COMPROMETIDO',	0,	'Dr. Hoyt Beier',	'SI',	24000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(8,	2,	'Chris',	'Morton Crist',	'Manuela Bradtke',	'1957-02-10',	'97098',	'friesen.maud@yahoo.com',	'2018-05-05 02:41:44',	'$2y$10$x4wkwsJfUd8v3Jyd7y/0DuVxtNih2Twxj0BMcujE9FPAABfVf0cOu',	NULL,	'MUJER',	'EN_RELACION',	1,	'Ms. Marie Schneider III',	'ALGO',	63000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(9,	3,	'Helena',	'Augusta Kozey',	'Mrs. Trinity Osinski Jr.',	'1954-05-08',	'66443',	'shea.langworth@yahoo.com',	'2019-08-30 11:17:52',	'$2y$10$xxiQgyxk3oST71vbTj3svuHD37E2tQ3wH2XRpncct4kCCabdGEEfC',	NULL,	'MUJER',	'DIVORCIADO',	0,	'Mr. Larue Boyer III',	'ALGO',	25000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(10,	3,	'Princess',	'Roma Glover Sr.',	'Tiana Skiles',	'1971-01-10',	'20136',	'talia17@medhurst.com',	'2019-02-18 08:50:19',	'$2y$10$WqPHj0VX2LD.AjbvUGC..uN7ngwVmlrtajCAKVeXZmc.R0hS/1bnO',	NULL,	'HOMBRE',	'VIUDO',	2,	'Gerard Deckow',	'ALGO',	30000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(11,	2,	'Floy',	'Mr. Uriel Metz Sr.',	'Jakob Shields II',	'2001-12-01',	'13021',	'tromp.cyrus@hotmail.com',	'2018-06-16 00:26:41',	'$2y$10$cVq8lRSlo3zVWfEzMrZw0elw6lQZSloiICcsqwgcg8.cj8oETQOpy',	NULL,	'MUJER',	'EN_RELACION',	0,	'Brown Harber',	'ALGO',	22000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(12,	3,	'Maryam',	'Jaren Upton',	'Maximo Connelly III',	'1984-01-21',	'07668',	'imclaughlin@bode.com',	'2019-01-07 12:41:17',	'$2y$10$DjznYAjl0HBZJ2bJAJB2a.yftfsfz5/.cogMZqNceSL8EBCLWn0Ly',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Anabel Jast',	'ALGO',	98000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(13,	3,	'Carmel',	'Mrs. Constance Goldner MD',	'Chad Von',	'1981-09-07',	'98551',	'elsie70@emmerich.com',	'2019-10-20 23:51:03',	'$2y$10$Xwa7bciYSFUo9bMKgxlEDOs5Kmk2s.XAvhW3vl5Pn3Kfj3Apbo7GG',	NULL,	'MUJER',	'COMPROMETIDO',	0,	'Adan Bosco',	'SI',	59000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(14,	2,	'Tamia',	'Mrs. Haven Wiza',	'Rylee Emard',	'1955-09-02',	'04047',	'isabell97@ohara.info',	'2019-12-05 05:46:34',	'$2y$10$AUTA0mUeUaYlr/Ya9.DHK.7/vOvyi53joHlN8RAwkZML4AJtyDCyS',	NULL,	'HOMBRE',	'DIVORCIADO',	4,	'Amiya O\'Reilly',	'NO',	33000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(15,	3,	'Newell',	'Heaven Hintz',	'Nathanael McKenzie',	'1978-03-24',	'54878',	'fwiza@yahoo.com',	'2019-03-23 16:47:16',	'$2y$10$eefvfO7LmPu8ekNOWWg97OULy05MEtLg67OoQA.jr5nwR4aAc1I/C',	NULL,	'MUJER',	'SOLTERO',	0,	'Guido Schmitt',	'NO',	51000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(16,	2,	'Mac',	'Sydnie Dicki',	'Maryam Prohaska',	'1956-06-02',	'17167',	'aschaden@yahoo.com',	'2018-11-24 12:21:53',	'$2y$10$MVpc0fGlHzyyw06NQCuqGeX9x.zOfTOxJrRWCx3zdYU9rrbXisyzC',	NULL,	'MUJER',	'COMPROMETIDO',	0,	'Mr. Olaf Huel II',	'NO',	14000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(17,	2,	'Giovanny',	'Anissa Eichmann',	'Mrs. Carmella Blick',	'2001-04-12',	'99809',	'cristal.volkman@hotmail.com',	'2018-12-22 08:12:28',	'$2y$10$zPveAEmBy/kABWQFwuo0w.mLXlZN4jwM30MfSIJBbXpPlr3ZFi80W',	NULL,	'MUJER',	'COMPROMETIDO',	1,	'Herminia Jaskolski',	'ALGO',	48000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(18,	2,	'Bertha',	'Dayne Casper',	'Mrs. Penelope Monahan',	'1970-07-07',	'42304',	'xdamore@gmail.com',	'2018-03-09 02:59:14',	'$2y$10$1k1iZ3wQnAXMR7KCq4CpH.NqdyuQ.lmmqRYFia4rdMiqgkivjuD/O',	NULL,	'MUJER',	'EN_RELACION',	1,	'Letha Windler',	'ALGO',	18000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(19,	2,	'Vella',	'Damian Von',	'Prof. Bud Wintheiser MD',	'1981-08-29',	'35131',	'wiley.schoen@gislason.com',	'2019-09-01 06:42:06',	'$2y$10$juSmKsAkSurBsRY5CzZRZeIhap5iWPE7ycq6Fkc9MQ7c0TLHW.LaC',	NULL,	'HOMBRE',	'PAREJA_DE_HECHO',	3,	'Elsa Welch',	'SI',	73000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(20,	3,	'Candace',	'Tavares Lockman',	'Sheldon Schmitt DVM',	'1995-06-02',	'36106',	'oscar71@steuber.com',	'2020-04-24 17:28:35',	'$2y$10$H6M3ZAlgdsxFB6n8nzY5eeZBZmhfTNHPD8Hac5huY.XcDE0GCBcz2',	NULL,	'HOMBRE',	'COMPROMETIDO',	1,	'Jan Towne',	'SI',	27000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(21,	3,	'Moises',	'Edmund Baumbach',	'Miss Georgette Schneider',	'1978-08-30',	'92820',	'nick34@gmail.com',	'2019-05-30 04:18:06',	'$2y$10$pu928rnShfo1bZnUUYVNbuCNhzSAB/HSfUUoaV48yyJuMBGVJjWwi',	NULL,	'MUJER',	'VIUDO',	2,	'Dayne Lueilwitz',	'NO',	37000,	'2020-06-13 11:26:33',	'2020-06-13 11:26:33'),
(22,	3,	'Catherine',	'Enoch Jast',	'Dr. Merritt Walter',	'1957-05-01',	'48468',	'ndaugherty@hudson.com',	'2018-05-09 20:42:50',	'$2y$10$Bsq5IcjwfX8h9cO1IwzHNO0MTs4603ymxwy6dyi4rzqWbZia2t6P2',	NULL,	'HOMBRE',	'DIVORCIADO',	0,	'Ima Kohler DDS',	'NO',	23000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(23,	2,	'Tess',	'Ms. Anna Bogisich',	'Zita Mante',	'1964-07-05',	'64018',	'kunze.abbie@hotmail.com',	'2019-12-21 21:35:19',	'$2y$10$R1gwtPOi8TmkrYoBy2p1heoz2IoD5tuYMkcvc5/Uko3vFTJ0t3V6m',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Jody Wehner',	'SI',	39000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(24,	3,	'Antonette',	'Mr. Brooks Hackett',	'Prof. Stuart Ullrich DDS',	'1974-11-18',	'78599',	'ykris@waelchi.com',	'2018-02-16 02:17:25',	'$2y$10$H2YIAj5TDuc3g7Q39jhgaeMUIS7.pZAXdaYtpC7UTN2vCLYlLyk52',	NULL,	'HOMBRE',	'PAREJA_DE_HECHO',	1,	'Davin Okuneva',	'NO',	17000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(25,	2,	'Elisa',	'Elouise Aufderhar',	'Ms. Reyna Jones I',	'1958-09-23',	'77146',	'howell.raymundo@hotmail.com',	'2020-05-05 02:59:59',	'$2y$10$ydwdnPx3UexwR17zwYz5EOV6DaxUv5neuqi3cyUqrzBhtUU3s.jyi',	NULL,	'HOMBRE',	'PAREJA_DE_HECHO',	2,	'Zula Erdman',	'ALGO',	60000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(26,	2,	'Adolphus',	'Dr. Charlie Altenwerth',	'Clinton Little',	'1977-10-12',	'09926',	'dana.veum@vandervort.info',	'2018-06-22 11:36:01',	'$2y$10$hApp/XK/KDGiR2m/22UWT.hJtKYwJEyvErWChaPID.o.A3o3.WouK',	NULL,	'HOMBRE',	'VIUDO',	2,	'Gianni Rogahn',	'ALGO',	84000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(27,	3,	'Lempi',	'Camille Wehner',	'Prof. Melissa Macejkovic I',	'1959-02-18',	'51357',	'schroeder.magali@cole.com',	'2020-05-12 03:36:40',	'$2y$10$VXv1kUSP8VZAm6HEGL.igODfCn0LTZzrZ0w1h3AEAGFjEAuelmUHu',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Prof. Cullen Zboncak',	'ALGO',	23000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(28,	3,	'Jillian',	'Evans Witting',	'Favian Pfeffer',	'1973-09-19',	'23094',	'torn@hotmail.com',	'2019-12-26 01:52:26',	'$2y$10$5szXAyaSiKKrzKGAeb4Hg.jXtoSGIka3kzau9M8onUUt7h5iVhRz.',	NULL,	'MUJER',	'EN_RELACION',	0,	'Gerardo Schaden',	'SI',	20000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(29,	2,	'Wendy',	'Sister Gleason',	'Jeffrey D\'Amore',	'1971-10-08',	'95416',	'slowe@gmail.com',	'2018-07-24 15:01:12',	'$2y$10$ADvkvxZS438T5zHZ5H5ppO3QVHuJlOdw54hKblMisq6jnmoHSf/SG',	NULL,	'HOMBRE',	'COMPROMETIDO',	0,	'Iliana Green',	'NO',	52000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(30,	2,	'Shea',	'Miss Piper Ondricka',	'Sam Bechtelar MD',	'1956-11-20',	'16366',	'eve.legros@yahoo.com',	'2019-10-07 15:48:25',	'$2y$10$knn4VlamcKsNKeLEPCj7Cuh98yvckrVhDgJn/bgqROKDW/Z.tRTIG',	NULL,	'HOMBRE',	'CASADO',	4,	'Dorian McClure',	'SI',	40000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(31,	2,	'Samir',	'Reyes Beer',	'Miss Ruthe McLaughlin III',	'1952-01-17',	'12495',	'altenwerth.heaven@farrell.org',	'2018-10-31 02:15:15',	'$2y$10$gsElAyxZzfXQr4jgbi0kMejnBi2yPYMNnvoQ.828ldvcQqCXx9BlK',	NULL,	'HOMBRE',	'VIUDO',	2,	'Dr. Lew Roob DVM',	'ALGO',	76000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(32,	3,	'Evalyn',	'Hobart Kris',	'Sally Kemmer',	'1994-09-14',	'81946',	'rachael20@yahoo.com',	'2019-11-17 17:29:52',	'$2y$10$x9ObIt1Whvd0DEQPa/JtseMb2RUHXO/7fqVljDlamAWxZc0bznW/2',	NULL,	'MUJER',	'EN_RELACION',	0,	'Mrs. Beth Welch DVM',	'NO',	31000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(33,	2,	'Eliseo',	'Prof. Sylvia Zemlak MD',	'Caleigh Blanda III',	'1978-03-22',	'20051',	'candelario90@gmail.com',	'2019-12-17 16:04:30',	'$2y$10$NcQse38OrqgfuosYu0jSNeSqR0wTZbxSuC/NF7J3mvpZjbDKb9NW2',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Lisette Rosenbaum',	'ALGO',	29000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(34,	2,	'Myrtle',	'Omer Roob Sr.',	'Miss Myrtice Koss',	'1964-02-09',	'37844',	'russel.hodkiewicz@nienow.com',	'2019-11-17 20:08:02',	'$2y$10$8.PTA7Ex7KzD5yZWvILo8uiD99CmvK/hg3I3yH24BqMyDXbdfQ2y6',	NULL,	'HOMBRE',	'EN_RELACION',	0,	'Mr. Nat Thompson',	'ALGO',	70000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(35,	3,	'Jason',	'Jackson Schulist Sr.',	'Mr. Jesus Weber MD',	'1972-12-29',	'23570',	'pschroeder@senger.info',	'2018-07-10 03:10:27',	'$2y$10$wkdmiV2afpkEp95JPGw2heXDm5Fo8wQD0MCccfOxwCEyF/bUUBUBm',	NULL,	'HOMBRE',	'SOLTERO',	0,	'Toney Feest V',	'SI',	18000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(36,	3,	'Travon',	'Mr. Ryder Farrell IV',	'Moses Abbott',	'1977-08-12',	'70950',	'gjerde@reichel.com',	'2018-07-20 20:41:57',	'$2y$10$71rJTV2hGg8IZhlgIArxl.Op8eHx3GExXdRqsNMSM.G7IxCxoQrYS',	NULL,	'MUJER',	'SEPARADO',	3,	'Dillon Conroy',	'SI',	16000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(37,	2,	'Jordi',	'Mr. Alek Jacobi',	'Brionna Ernser',	'1988-08-12',	'77672',	'gstreich@gmail.com',	'2018-06-08 04:03:00',	'$2y$10$uRHAni3a1RKzWXRF/mwOE.iChAm8r1vY3HW9N/LjQ7Er76jTIFdIu',	NULL,	'MUJER',	'PAREJA_DE_HECHO',	3,	'Marisa Kemmer',	'ALGO',	41000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(38,	3,	'Hertha',	'Catherine Jaskolski',	'Tate Stiedemann I',	'1950-05-28',	'53025',	'volkman.destini@dietrich.com',	'2018-09-16 10:41:22',	'$2y$10$YjWyIxo4xT/57B1KpgGrkOjBOndY5clx09ukx20wVpr0KQqSRsgxG',	NULL,	'MUJER',	'SOLTERO',	0,	'Courtney Stroman',	'SI',	21000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(39,	2,	'Patricia',	'Devyn Larkin',	'Keenan Pagac',	'1997-03-04',	'88007',	'abigale.kulas@gmail.com',	'2018-11-11 05:58:03',	'$2y$10$DIvuabra2LTwskqdqlNu9Ou3wpDQPOmJmghA4Uq.vWhdS98ThBR9.',	NULL,	'MUJER',	'EN_RELACION',	0,	'Dr. Romaine Bernier',	'NO',	57000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(40,	2,	'Ada',	'Judge Kreiger',	'Cleo Waters V',	'1999-10-22',	'00464',	'dwight34@collier.com',	'2018-12-29 21:26:49',	'$2y$10$J/w98no6.5/fS3S22cJWaeZRtNHHbT/cAgWTxlozys/VqPXDFM15u',	NULL,	'HOMBRE',	'CASADO',	4,	'Claudie Powlowski',	'ALGO',	67000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(41,	2,	'Erich',	'Rupert Corwin',	'Alda Treutel DDS',	'2000-03-23',	'03123',	'khill@gmail.com',	'2019-04-26 06:03:24',	'$2y$10$w51Jq6imEfHm/CFRYvAWGeXzt9rYcqiWYZThVp8xt1tNFsx8H2FMO',	NULL,	'HOMBRE',	'CASADO',	2,	'Levi Parker PhD',	'ALGO',	78000,	'2020-06-13 21:14:11',	'2020-06-13 21:14:11'),
(42,	1,	'Mikel',	'Prieto',	'Sánchez',	NULL,	'48510',	'mklprt@gmail.com',	NULL,	'$2y$10$NA9UX1XaEg7Q4hbdZT2/m.JMNgvvQA0uXVsLLIi.dkqWjQpxI0HgW',	'ltF9TUfki8k7OPloMn7HC58C4jHHqIWL4PUaNOiioTTx1fgjR1pMCDMj5D7y',	NULL,	'COMPROMETIDO',	0,	'Ninguno',	NULL,	12500,	'2020-06-13 22:21:13',	'2020-07-05 14:55:17');

-- 2020-07-08 17:25:36
