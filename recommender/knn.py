# ./recommender/env/bin/python3 ./recommender/main.py <user-id>
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import NearestNeighbors
import argparse
from collections import Counter
import sys

MAX_RECOMMENDATIONS = 10
FILTERING = 15

# Ejemplo:
# Entrada : '[1, 1, 5, 126998, 5, 2019, \'GASOLINA\', \'NUEVO\', 0, 625, \'MANUAL\', 10.5, 240.0]'
# Salida : '[1.0, 0.718927644854425, 0.0004998056311434351, 0.0, 1.0, 0.5898876404494382, 0.802675585284281, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0]'

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('user_id', type=int, help='User ID')
	args = parser.parse_args()

	df = pd.read_csv('../recommender/products.csv')

	# Solo los activos
	df = df.loc[df['active'] == 1]

	df = df[
		['id', 'brand_id', 'id_car_type', 'price', 'num_doors', 'manufacturing_year', 'fuel', 'status', 'kilometers',
		 'horsepower', 'gear_shift', 'fuel_consumption_at_100', 'co2_emission']]

	df[['brand_id', 'id_car_type', 'num_doors', 'fuel', 'status', 'gear_shift']] = df[
		['brand_id', 'id_car_type', 'num_doors', 'fuel', 'status', 'gear_shift']].astype('category')

	df = pd.get_dummies(df)
	
	# One hot encoding
	# Se discretizan todos los atributos en la misma escala (del 0 al 1) para que las distancias entre ellos sean comparables
	scaler = MinMaxScaler()
	df[['price', 'manufacturing_year', 'kilometers', 'horsepower', 'fuel_consumption_at_100', 'co2_emission']] =\
		scaler.fit_transform(df[['price', 'manufacturing_year', 'kilometers', 'horsepower', 'fuel_consumption_at_100',
								 'co2_emission']])

	# watched_cars
	df_user = pd.read_csv('../recommender/watched_cars.csv')
	df_user = df_user.loc[df_user['id_user'] == args.user_id]

	#Si el usuario no ha visto coches devuelve una lista vacía: No se puede recomendar
	user_cars = list(df_user['id_car'])
	if len(user_cars) == 0 : 
		print(list())
		sys.exit(0)

	# Se filtran los coches vistos, con la misma estructura que arriba
	user_cars = df.loc[df['id'].isin(user_cars)]

	# Se saca los N=FILTERING vecinos cercanos con radio de 1.4
	neigh = NearestNeighbors(n_neighbors=FILTERING, radius=1.4)
	neigh.fit(df.loc[:, df.columns != 'id'])  # Sin ID
	recommendations = neigh.kneighbors(user_cars.loc[:, user_cars.columns != 'id'], FILTERING, return_distance=False)
	if len(recommendations) == 0 : 
		print(list())
		sys.exit(0)
	recommendations = recommendations.flatten()

	# Se cuentan las recomendaciones por puntos
	recommendations = Counter(recommendations)
	recommendations = [(k, v) for k, v in recommendations.items()]

	# Se cogen las que mas puntos tengan
	recommendations = sorted(recommendations, key=lambda x: x[1])[::-1]
	recommendations = recommendations[0:min(len(recommendations), MAX_RECOMMENDATIONS)]

	# Se hace lookup del index al ID del coche
	recommendations = [int(df.iloc[recommendation[0]]['id']) for recommendation in recommendations]

	# Se sacan por consola
	print(recommendations)