<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->unsigned()->default(2);
            $table->string('name')->default('Customer');
            $table->string('surname1')->default('');
            $table->string('surname2')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->enum('gender', ['HOMBRE', 'MUJER'])->nullable();
            $table->enum('marital_status', ['SOLTERO','COMPROMETIDO','EN_RELACION','CASADO','PAREJA_DE_HECHO','SEPARADO','DIVORCIADO','VIUDO'])->nullable();
            $table->integer('num_children')->nullable();
            $table->string('current_car')->nullable();
            $table->enum('eco_interest', ['SI','ALGO','NO'])->nullable();
            $table->integer('annual_income')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        });

        Schema::table('users', function($table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
