<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('brand_id')->unsigned();
            $table->integer('id_car_type')->unsigned();
            $table->string('title', 75);
            $table->string('slug', 100);
            $table->string('short_description', 300);
            $table->string('description', 2000);
            $table->string('img_route', 300);
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('num_doors');
            $table->integer('manufacturing_year');
            $table->enum('fuel', ['GASOLINA','DIESEL','HIBRIDO','ELECTRICO']);
            $table->enum('status', ['NUEVO','KM0','USADO']);
            $table->integer('kilometers')->default(0);
            $table->integer('horsepower');
            $table->enum('gear_shift', ['AUTOMATICO','MANUAL']);
            $table->float('fuel_consumption_at_100');
            $table->float('co2_emission');
            $table->integer('active')->default(1);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        });

        Schema::table('products', function($table) {
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->foreign('id_car_type')->references('id')->on('car_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
