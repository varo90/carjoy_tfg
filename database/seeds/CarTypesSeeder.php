<?php

use Illuminate\Database\Seeder;

class CarTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_types')->insert([
            ['name' => 'Familiar'],
            ['name' => 'Sedán'],
            ['name' => 'Deportivo'],
            ['name' => 'Monovolumen'],
            ['name' => 'Coupé'],
            ['name' => 'Todoterreno'],
            ['name' => 'SUV'],
        ]);
    }
}
