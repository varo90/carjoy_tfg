<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = array();
        for($i=0;$i<100;$i++) {
            array_push($products,[
                'brand_id' => 1,
                'id_car_type' => 5,
                'title' => 'BMW M8 Gran Coupé',
                'slug' => Str::slug('BMW M8 Gran Coupé'),
                'short_description' => 'El BMW M8 ofrece el más alto nivel de deportividad y elegancia de toda la gama BMW, y el nuevo BMW M8 Gran Coupé es el más exótico y fascinante de todos ellos.',
                'img_route' => 'images/cars/bmw_m8_gran_coupe.jpg',
                'description' => 'El diseño avanzado del nuevo BMW M8 Competition Gran Coupé impresiona por el radical dinamismo de sus contornos, que son a la vez elegantes. El resultado es un impresionante deportivo de cuatro puertas que transmite con fluidez su carácter exclusivo desde cualquier ángulo. Utilizando los materiales más refinados y un impecable acabado, el interior presenta la simbiosis perfecta entre puro estilo deportivo y lujo exclusivo.',
                'quantity' => 1,
                'price' => 126998,
                'num_doors' => 5,
                'manufacturing_year' => 2019,
                'fuel' => 'GASOLINA',
                'status' => 'NUEVO',
                'kilometers' => 0,
                'horsepower' => 625,
                'gear_shift' => 'MANUAL',
                'fuel_consumption_at_100' => 10.5,
                'co2_emission' => 240,
                'active' => 1
            ]);
        }
        DB::table('products')->insert($products);
        // DB::table('products')->insert([
        //     [
        //         'brand_id' => 1,
        //         'id_car_type' => 5,
        //         'title' => 'BMW M8 Gran Coupé',
        //         'slug' => Str::slug('BMW M8 Gran Coupé'),
        //         'short_description' => 'El BMW M8 ofrece el más alto nivel de deportividad y elegancia de toda la gama BMW, y el nuevo BMW M8 Gran Coupé es el más exótico y fascinante de todos ellos.',
        //         'img_route' => 'images/cars/bmw_m8_gran_coupe.jpg',
        //         'description' => 'El diseño avanzado del nuevo BMW M8 Competition Gran Coupé impresiona por el radical dinamismo de sus contornos, que son a la vez elegantes. El resultado es un impresionante deportivo de cuatro puertas que transmite con fluidez su carácter exclusivo desde cualquier ángulo. Utilizando los materiales más refinados y un impecable acabado, el interior presenta la simbiosis perfecta entre puro estilo deportivo y lujo exclusivo.',
        //         'quantity' => 1,
        //         'price' => 126998,
        //         'num_doors' => 5,
        //         'manufacturing_year' => 2019,
        //         'fuel' => 'GASOLINA',
        //         'status' => 'NUEVO',
        //         'kilometers' => 0,
        //         'horsepower' => 625,
        //         'gear_shift' => 'MANUAL',
        //         'fuel_consumption_at_100' => 10.5,
        //         'co2_emission' => 240,
        //         'active' => 1
        //     ]
        // ]);
    }
}
