<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Provider\Address;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'alvaro_sm_8@usal.es')->exists()) {
            DB::table('users')->insert([
                'role_id' => 1,
                'name' => 'Álvaro',
                'surname1' => 'Santos',
                'surname2' => 'Martín',
                'date_of_birth' => Carbon::parse('1990-12-30'),
                'zip_code' => '37002',
                'email' => 'alvaro_sm_8@usal.es',
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('123456'),
                'gender' => 'HOMBRE',
                'marital_status' => 'SOLTERO',
                'num_children' => 0,
                'current_car' => 'Nissan Almera',
                'eco_interest' => 'SI',
                'annual_income' => 20000
            ]);
        }

        $faker = Faker\Factory::create();
        $users = array();
        for($i=0;$i<20;$i++) {
            $timestamp = mt_rand(strtotime('1950-01-01'), strtotime('2002-01-01'));
            $randomDate1 = date("d-M-Y", $timestamp);
            $timestamp = mt_rand(strtotime('2018-01-01'), strtotime('now'));
            $randomDate2 = date("d-M-Y H:i:s", $timestamp);
            $gender = $faker->randomElement(['HOMBRE', 'MUJER']);
            $marital_status = $faker->randomElement(['SOLTERO','COMPROMETIDO','EN_RELACION','CASADO','PAREJA_DE_HECHO','SEPARADO','DIVORCIADO','VIUDO']);
            if($marital_status == 'SOLTERO') {
                $num_children = 0;
            }
            else if($marital_status == 'COMPROMETIDO' || $marital_status == 'EN_RELACION') {
                $num_children = $faker->numberBetween(0,1);
            } else {
                $num_children = $faker->numberBetween(0,4);
            }
            $annual_income_chance = $faker->numberBetween(0,18);
            if($annual_income_chance <= 5) {
                $annual_income = $faker->numberBetween(12,25);
            } else if($annual_income_chance >= 6 && $annual_income_chance <= 10) {
                $annual_income = $faker->numberBetween(26,45);
            } else if($annual_income_chance >= 11 && $annual_income_chance <= 14) {
                $annual_income = $faker->numberBetween(46,65);
            } else if($annual_income_chance >= 15 && $annual_income_chance <= 17) {
                $annual_income = $faker->numberBetween(66,85);
            } else if($annual_income_chance == 18) {
                $annual_income = $faker->numberBetween(86,100);
            }

            array_push($users,[
                'role_id' => $faker->numberBetween(2,3),
                'name' => $faker->firstName($gender),
                'surname1' => $faker->name,
                'surname2' => $faker->name,
                'date_of_birth' => Carbon::parse($randomDate1),
                'zip_code' => Address::postcode(),
                'email' => $faker->unique()->email,
                'email_verified_at' => Carbon::parse($randomDate2),
                'password' => bcrypt('123456'),
                'gender' => $gender,
                'marital_status' => $marital_status,
                'num_children' => $num_children,
                'current_car' => $faker->name,
                'eco_interest' => $faker->randomElement(['SI', 'ALGO', 'NO']),
                'annual_income' => $annual_income*1000
            ]);
        }

        DB::table('users')->insert($users);
    }
}