<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            ['name' => 'BMW'],
            ['name' => 'Mercedes'],
            ['name' => 'Nissan'],
            ['name' => 'Seat'],
            ['name' => 'Citroën']
        ]);
    }
}
